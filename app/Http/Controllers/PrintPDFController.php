<?php

namespace App\Http\Controllers;

use App\Models\TestDetails;
use App\Models\WebPages;
use App\Traits\FifthStep;
use App\Traits\FirstStepTrait;
use App\Traits\FourthStep;
use App\Traits\IterationTrait;
use App\Traits\PageTrait;
use App\Traits\SecondStep;
use App\Traits\sixthStep;
use App\Traits\StepsTrait;
use App\Traits\ThirdStep;
use App\Traits\UserLoadTrait;
use Illuminate\Http\Request;
use PDF;

class PrintPDFController extends Controller
{
    use PageTrait,IterationTrait,StepsTrait,FirstStepTrait,
        SecondStep,ThirdStep,FourthStep,FifthStep,
        UserLoadTrait,sixthStep;
    //
    public function downLoadUserExperiencePlane()
    {
        $loadInfo=$this->getAllUserLoad();

        $testName= "Simulate User Experience";
        $website=$loadInfo['websiteName'];
        $iterations=$this->getAllStoredIterations();
        $pages=$this->getAllPages();

        $normalUser='normalUser';
        $rampUpTime=$loadInfo['normal']['duration']*60;
        $expectedThroughputInNormal= $this->getExpectedThroughputForPages($pages,$iterations,$normalUser,$rampUpTime);

        $peakUser='peakUser';
        $rampUpTime=$loadInfo['peak']['duration']*60;
        $expectedThroughputInPeak= $this->getExpectedThroughputForPages($pages,$iterations,$peakUser,$rampUpTime);

        $futureUser='futureUser';
        $rampUpTime=$loadInfo['future']['duration']*60;
        $expectedThroughputInFuture= $this->getExpectedThroughputForPages($pages,$iterations,$futureUser,$rampUpTime);

        $testID=$loadInfo['testID'];
//        dd($testID);

//        dd($expectedThroughputInNormal,$expectedThroughputInPeak,$expectedThroughputInFuture);
//        dd($iterations,$pages);
//        return view('PDF.testPlane', compact('testName','website','loadInfo','iterations','pages'));
        $pdf =   PDF::loadView('PDF.testPlane', compact('testName','website','loadInfo','iterations','pages','testID'));
//        $view = view('PDF.testPlane', compact('testName','website'))->render();
//        $pdf = \PDF::loadHTML($view)->setPaper('a4', 'potrait')->setWarnings(false)->save('myfile.pdf');
//        dd($ciy,$pdf);
//dd("dd");
        $pdfName=$website."UserExperiencePlane.pdf";
        return $pdf->download($pdfName);
    }

    public function downLoadTestReport($testID)
    {
        $testInfo=TestDetails::find($testID);
        $websiteNameReporting=$testInfo->websiteName;
        $storedResults=WebPages::with(['pageResults','pageJMeterResults','pageImprovement'])
            ->where('testID',$testInfo->id)
            ->get()->toArray();
//        return view('PDF.report', compact('testInfo','websiteNameReporting','storedResults'));
        $pdf =   PDF::loadView('PDF.report', compact('testInfo','websiteNameReporting','storedResults'));
        $pdfName=$websiteNameReporting."FinalTestReport.pdf";
//        sleep(10);
        return $pdf->download($pdfName);
//        dd($testID);
    }
}
