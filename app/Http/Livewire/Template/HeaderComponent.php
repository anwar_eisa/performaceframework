<?php

namespace App\Http\Livewire\Template;

use Livewire\Component;

class HeaderComponent extends Component
{
    public function render()
    {
        return view('livewire.template.header-component');
    }
}
