<?php

namespace App\Http\Livewire\Template;

use App\Traits\FifthStep;
use App\Traits\FirstStepTrait;
use App\Traits\FourthStep;
use App\Traits\IterationTrait;
use App\Traits\PageTrait;
use App\Traits\SecondStep;
use App\Traits\sixthStep;
use App\Traits\StepsTrait;
use App\Traits\ThirdStep;
use App\Traits\UserLoadTrait;
use Barryvdh\DomPDF\PDF;
use Livewire\Component;
use PhpScience\PageRank\Builder\NodeBuilder;
use PhpScience\PageRank\Builder\NodeCollectionBuilder;
use PhpScience\PageRank\Service\PageRankAlgorithm;
use PhpScience\PageRank\Service\PageRankAlgorithm\Normalizer;
use PhpScience\PageRank\Service\PageRankAlgorithm\RankComparator;
use PhpScience\PageRank\Service\PageRankAlgorithm\Ranking;
use PhpScience\PageRank\Strategy\MemorySourceStrategy;
use Illuminate\Support\Arr;

class HomeComponent extends Component
{
    use PageTrait,IterationTrait,StepsTrait,FirstStepTrait,
        SecondStep,ThirdStep,FourthStep,FifthStep,
        UserLoadTrait,sixthStep;
    public $dataSource=[];
    public $pages=[1];
    public $pagesName;
    public $pagesResponseTime;
    public $pagesThinkTime;
    public $i=1;
    public $zeroStep=true;
    public $firstStep=false;
    public $secondStep=false;
    public $thirdStep=false;
    public $fourthStep=false;
    public $fifthStep=false;
    public $sixthStep=false;
    public $seventhStep=false;
//    public $showPath=false;
    public $pageIN;
    public $allPagesInSecond;
    public $allPagesInFirst;
    public $firstPageInSecond;
    public $firstKeyInSecond;
    public $lastKeyInSecond;
    public $pageOut;
    public $savedOut=false;
    public $savedIn=false;
    public $firstNode;
    public $endNode;
    public $dataSourceCollection;
    public $iterationsGenerated=[];
    public $selectedIterations=[];
    public $pathDiscoverResult=false;
    public $pacing;
    public $iterationLoop;

    public $normalUserLoad;
    public $normalLoadDuration;
    public $peakUserLoad;
    public $peakLoadDuration;
    public $futureUserLoad;
    public $futureLoadDuration;
    public $enduranceDuration;
    public $websiteName;
    public $loadInfo;
    public $throughPages;
    public $website;
    public $testName;
    public $testID;
    public $iterationsInFifth=[];
    public $pagesInFifth;
    public $testIDReporting;
    public $resultsFromJMeter=false;
    public $resultsFromJTMitrex=false;
    public $storedPages;
    public $loadTime;
    public $firstByte;
    public $startRender;
    public $firstContentfulPaint;
    public $speedIndex;
    public $largestContentfulPaint;
    public $cumulativeLayoutShift;
    public $blockingTime;
    public $timeInteractive;
    public $GTPerformance;
    public $GTStructure;
    public $improvement=[];
    public $pageResultCounter=0;
    public $jmeterResultCounter=0;
    public $improvementCounts=[1];
    public $loadTimeDes;
    public $firstByteDEs;
    public $startRenderDes;
    public $firstContentfulPaintDes;
    public $speedIndexDes;
    public $largestContentfulPaintDes;
    public $cumulativeLayoutShiftDes;
    public $timeInteractiveDes;
    public $blockingTimeDes;
    public $selectedScenario;
    public $minResponseTime;
    public $maxResponseTime;
    public $medianResponseTime;
    public $responseTime90th;
    public $responseTime95th;
    public $responseTime99th;
    public $errorRatio;
    public $throughput;
    public $selectedPageResultJmeter;
    public $addedGTMitrics;
    public $addedJmeters;
    public $selectedTestID;
    public $selectedPageResultGTMitrics;
    public $websiteNameReporting;
    public $storedResults=[];
    public $testInfo;



    protected $listeners = ['newPathDiscovered'];



    private function getDataSource(): array
    {
        //example1
//        return [
//            1 => [
//                'id'  => 1,
//                'out' => [2, 3],
//                'in'  => [3]
//            ],
//            2 => [
//                'id'  => 2,
//                'out' => [4],
//                'in'  => [1, 3]
//            ],
//            3 => [
//                'id'  => 3,
//                'out' => [1, 2, 4],
//                'in'  => [1, 4]
//            ],
//            4 => [
//                'id'  => 4,
//                'out' => [3],
//                'in'  => [3, 2]
//            ]
//        ];
        //example2
        return [
            1 => [
                'id'  => 1,
                'name'=>"home",
                'out' => [2],
                'outName' => ["country"],
                'in'  => [2,3,5],
                'rank'=>10,
            ],
            2 => [
                'id'  => 2,
                'name'=>"country",
                'out' => [1,3],
                'outName' => ["home","city"],
                'in'  => [1],
                'rank'=>9,

            ],
            3 => [
                'id'  => 3,
                'name'=>"city",
                'out' => [1, 4,5],
                'outName' => ["home", "service"],
                'in'  => [2],
                'rank'=>8,

            ],
            4 => [
                'id'  => 4,
                'name'=>"service",
                'out' => [5],
                'outName' => ["restaurant"],
                'in'  => [3, 5],
                'rank'=>2,

            ],
            5 => [
                'id'  => 5,
                'name'=>"restaurant",
                'out' => [1,4],
                'outName' => ["home","restaurant"],
                'in'  => [4],
                'rank'=>1,

            ]
        ];
    }


    public function getRankOfAllNodes()
    {
//        dd($this->firstKeyInSecond,$this->allPagesInSecond);
//        $this->dataSource = $this->getDataSource();
        $this->dataSource=$this->getAllPages();
//        $old= $this->getDataSource();
//        dd($this->dataSource,$old);

//        dd($this->>dataSource[1]['id']);
        $nodeBuilder = new NodeBuilder();
//        dd($nodeBuilder);
        $nodeCollectionBuilder = new NodeCollectionBuilder();
        $strategy = new MemorySourceStrategy(
            $nodeBuilder,
            $nodeCollectionBuilder,
//            $old
            $this->dataSource
        );
//        dd($strategy->nodeListMap[1]['id']);

        $rankComparator = new RankComparator();
//        dd($rankComparator);
        $ranking = new Ranking(
            $rankComparator,
            $strategy
        );
//        dd($ranking->nodeDataStrategy->nodeListMap[1]['id']);

        $normalizer = new Normalizer();
//        dd($normalizer);

        $pageRankAlgorithm = new PageRankAlgorithm(
            $ranking,
            $strategy,
            $normalizer
        );
//        dd($pageRankAlgorithm->nodeDataStrategy->nodeListMap[1]['id']);

        $maxIteration = 100;
        $nodeCollection = $pageRankAlgorithm->run($maxIteration);

        $nods=$nodeCollection->getNodes();
//        dd($nods);
//        $nods=$nods->sortByDesc('rank');
        $nods = collect($nods)->sortByDesc('rank')->toArray();
//        $orderNods=[];
        foreach ($nods as $key=>$n)
        {
            $this->dataSource[$n->id]['rank']=$n->rank;
//            $orderNods[$key]= $n->id;
        }
        $this->store($this->dataSource);

//        dd($nods,$this->dataSource);

//        var_dump($nodeCollection->getNodes());
    }

    public function stopOnErrors()
    {
        return;
    }

    public function mount()
    {
//        $this->clearAllGeneratedIterations();
//        $this->clearAllStoredIterations();
//        $this->getRankOfAllNodes();
//        $this->dataSource=$this->getDataSource();
//        $this->store($data);
//        $this->dataSource=$this->getAllPages();
//        $this->clearAllGeneratedIterations();
//        $this->clearAllStoredIterations();

//        $this->dataSource=[];
//        $this->dataSource=$this->sortDataSourceByRank($dataSource);

//        dd($this->dataSource,$rank);


//        dd($this->dataSource);
//        dd(count($this->iterationsGenerated));
//        $this->dataSource = collect($this->dataSource)->sortByDesc('rank')->toArray();


    }
    public function render()
    {
        return view('livewire.template.home-component');
    }



}
