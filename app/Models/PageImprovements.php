<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageImprovements extends Model
{
    use HasFactory;
    protected $fillable = ['improvements','pageID'];
    public function webPage()
    {
        return $this->belongsTo('App\Models\WebPages','pageID');
    }
}
