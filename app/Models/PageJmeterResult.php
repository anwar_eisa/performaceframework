<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageJmeterResult extends Model
{
    use HasFactory;
    protected $fillable = ['scenario','error','min','max',
        'median','90th','95th','99th','throughput','pageID',
        'deviation','deviation2','average','testID'];
    public function webPage()
    {
        return $this->belongsTo('App\Models\WebPages','pageID');
    }
    public function testDetails()
    {
        return $this->belongsTo('App\Models\TestDetails','testID');
    }
}
