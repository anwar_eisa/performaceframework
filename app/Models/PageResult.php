<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageResult extends Model
{
    use HasFactory;
    protected $fillable = ['standard','value','details','pageID','testID'];

    public function webPage()
    {
        return $this->belongsTo('App\Models\WebPages','pageID');
    }

    public function testDetails()
    {
        return $this->belongsTo('App\Models\TestDetails','testID');
    }
}
