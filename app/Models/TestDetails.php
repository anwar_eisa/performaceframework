<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestDetails extends Model
{
    use HasFactory;

    protected $fillable = ['testID','websiteName'];

    public function pages()
    {
        return $this->hasMany('App\Models\WebPages','testID');
    }

    public function jmeterResults()
    {
        return $this->hasMany('App\Models\PageJmeterResult','testID');
    }

    public function gtmetricResults()
    {
        return $this->hasMany('App\Models\PageResult','testID');
    }

}
