<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebPages extends Model
{
    use HasFactory;
    protected $fillable = ['pageName','responseTime','testID'];
    public function testDetails()
    {
        return $this->belongsTo('App\Models\TestDetails','testID');
    }
    public function pageResults()
    {
        return $this->hasMany('App\Models\PageResult','pageID');
    }
    public function pageImprovement()
    {
        return $this->hasMany('App\Models\PageImprovements','pageID');
    }
    public function pageJMeterResults()
    {
        return $this->hasMany('App\Models\PageJmeterResult','pageID');
    }
}
