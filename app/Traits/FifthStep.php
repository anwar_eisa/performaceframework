<?php

namespace App\Traits;
trait FifthStep
{
    public function getUsersForIterations()
    {
        $sumOfRanksForPages=$this->getAverageRankForIterations();
        $pages=$this->getAllPages();
        $selectedIteration=$this->getAllStoredIterations();
        $selectedIterationAfterNormalize=$this->normalizeAverageOfRank($selectedIteration,$sumOfRanksForPages);
        $userLoad=$this->getAllUserLoad();
        $normalUserLoad=$userLoad['normal']['user'];
        $peakUserLoad=$userLoad['peak']['user'];
        $futureUserLoad=$userLoad['future']['user'];
        $enduranceUserLoad=$userLoad['endurance']['user'];

        foreach ($selectedIterationAfterNormalize as $key=>$iteration)
        {

            $normalUserLoadForIteration=$iteration['averageOfRankNormalized']*$normalUserLoad;
            $selectedIterationAfterNormalize[$key]['normalUser']=$normalUserLoadForIteration;
            $peakUserLoadForIteration=$iteration['averageOfRankNormalized']*$peakUserLoad;
            $selectedIterationAfterNormalize[$key]['peakUser']=$peakUserLoadForIteration;
            $futureUserLoadForIteration=$iteration['averageOfRankNormalized']*$futureUserLoad;
            $selectedIterationAfterNormalize[$key]['futureUser']=$futureUserLoadForIteration;
            $enduranceUserLoadForIteration=$iteration['averageOfRankNormalized']*$enduranceUserLoad;
            $selectedIterationAfterNormalize[$key]['enduranceUser']=$enduranceUserLoadForIteration;

//            dd($iteration,$normalUserLoad,$selectedIterationAfterNormalize);
        }
        $this->storeStoredIterations($selectedIterationAfterNormalize);
//        $this->validatNormalize();

//        dd($selectedIterationAfterNormalize);
    }
    public function getAverageRankForIterations()

    {
        $pages=$this->getAllPages();
        $selectedIteration=$this->getAllStoredIterations();
        $sumOfAverageOfRank=0;
        foreach ($selectedIteration as $key=>$iteration)
        {
            $sumOfRanksForPages=0;
            $counter=0;
            foreach ($iteration['pages'] as $page)
            {
                $sumOfRanksForPages +=$pages[$page]['rank'];
                $counter++;
            }
            $averageOfRank=$sumOfRanksForPages/$counter;
            $selectedIteration[$key]['averageOfRank']=$averageOfRank;
            $this->storeStoredIterations($selectedIteration);
            $sumOfAverageOfRank +=$averageOfRank;
//            dd($sumOfRanksForPages,$counter,$averageOfRank,$pages,$selectedIteration);

        }
        return $sumOfAverageOfRank;
//        dd($pages,$selectedIteration);
    }

    public function normalizeAverageOfRank($iterations,$sumOfAverage)
    {
//                dd($iterations,$sumOfAverage);

        foreach ($iterations as $key=>$iteration)
        {
//            dd($iterations[$key]);
            $normalizedValue=$iterations[$key]['averageOfRank']/$sumOfAverage;
//            dd($normalizedValue,$sumOfAverage,$iterations[$key]['averageOfRank']);
            $iterations[$key]['averageOfRankNormalized']=$normalizedValue;
        }
        return $iterations;
//        dd($iterations,$sumOfAverage);
    }

    public function getExpectedThroughputForPages($pages,$iterations,$normalUser,$rampUpTime)
    {
//        dd($rampUpTime,$normalUser);
        $expectedThroughput=[];

        foreach ($pages as $key=>$page)
        {

            $pacing=0;
            $usersInPage=0;
            $pageRepeatInIterations=0;
            $responseTime=0;
            $thinkTime=0;
            foreach ($iterations as $iteration)
            {
//                dd($key,$iteration['pages'],$iteration[$normalUser],$normalUser);

                if(in_array($page['id'],$iteration['pages']))
                {
                    $usersInPage +=round($iteration[$normalUser]);
                    $pacing += $iteration['pacing'];
                    $pageRepeatInIterations++;
                    $responseTime=$page['responseTime'];
                    $thinkTime=$page['thinkTime'];
                }
            }
            if($pageRepeatInIterations!=0)
            {
                $pacing=$pacing/$pageRepeatInIterations;
                $totalTime=$pacing+$responseTime+$thinkTime+$rampUpTime;
                $expectedThroughput[$page['id']]=$usersInPage/$totalTime;

            }


//            dd($usersInPage,$pacing,$pageRepeatInIterations,$iterations,$responseTime,$thinkTime,$expectedThroughput);

        }
        return $expectedThroughput;
//        dd($expectedThroughput);


//        dd($pages,$iterations);
    }

    public function validatNormalize()
    {
        $selectedIteration=$this->getAllStoredIterations();
        $userLoad=$this->getAllUserLoad();
        $normalUserLoad=$userLoad['normal']['user'];
        $peakUserLoad=$userLoad['peak']['user'];
        $futureUserLoad=$userLoad['future']['user'];
        $normalSum=0;
        $peakUser=0;
        $futureUser=0;
        foreach ($selectedIteration as $item)
        {
//            dd($item['normalUser']);
            $normalSum += $item['normalUser'];
            $peakUser += $item['peakUser'];
            $futureUser += $item['futureUser'];

        }
//        dd($selectedIteration,$normalUserLoad,$normalSum,$peakUserLoad,$peakUser,$futureUserLoad,$futureUser);

    }




}
