<?php

namespace App\Traits;
trait FirstStepTrait
{

    public function addNextPage($i)
    {
//        $newPages=$this->getAllPages();

//        dd($this->pagesName,$this->i,$newPages);
        if($this->pagesName==null)
        {
            $m="Please Enter Page Name First";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return;
        }

        if(!isset($this->pagesName[$this->i]))
        {
            $m="Please Enter Page Name First";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return;
        }
        if(!isset($this->pagesResponseTime[$this->i]))
        {
            $m="Please Enter Response Time For This Page";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return;
        }
        if(!isset($this->pagesThinkTime[$this->i]))
        {
            $m="Please Enter Think Time For This Page";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return;
        }
        $i=$i+1;
        $this->i=$i;
        array_push($this->pages,$i);
//        dd($this->pagesName);
        $this->saveAllPages($this->pagesName,$this->pagesResponseTime,$this->pagesThinkTime);
        $this->allPagesInFirst=$this->getAllPages();
//        request()->session()->put('pages', $this->pagesName);
//        $this->pageInSession=$this->pagesName;
//        dd($this->pagesName);
    }

    public function removePage($i)
    {

        $pages=$this->getAllPages();
//        dd($pages);
        $newPages=[];
//        dd($pages[$i]);
        if(isset($pages[$i]))
        {
            unset($pages[$i]);
        }
        if(isset($this->pagesName[$i]))
        {
            unset($this->pagesName[$i]);
        }

        foreach ($pages as $key=>$page)
        {
            if(isset($page['in']))
            {
                unset($page['in']);
            }
            if(isset($page['out']))
            {
                unset($page['out']);

            }
            if( isset($page['rank']))
            {
                unset($page['rank']);
            }
            $newPages[$key]=$page;

        }

        $pageSorted=$this->resortDataSourceAfterDeletePage($newPages);
//        dd($pageSorted);
        $this->store($pageSorted);
        $this->allPagesInFirst=$this->getAllPages();
        $this->clearAllGeneratedIterations();
        $this->clearAllStoredIterations();

    }

    public function deleteAllPages()
    {
        $this->clearAllPages();
        $this->clearAllGeneratedIterations();
        $this->clearAllStoredIterations();
        $this->clearAllUserLoad();
        $this->pagesName=null;
        $this->i=1;
        $this->pages=[1];
        $this->pagesThinkTime=null;
        $this->pagesResponseTime=null;
        $this->allPagesInFirst=$this->getAllPages();
    }
    public function resortDataSourceAfterDeletePage($pages)
    {
//        $pages=$this->getAllPages();
        $newPages=[];
        $id=1;
        foreach ($pages as $page)
        {
            if($id!=$page['id'])
            {
                $page['id']=$id;
            }
            $newPages[$id]=$page;
            $id++;
        }
        return $newPages;
//        dd($pages,count($pages),$newPages);
    }



    public function validateUserLoad($websiteName,$normalUserLoad,$normalLoadDuration,$peakUserLoad,$peakLoadDuration,$futureUserLoad,$futureLoadDuration,$enduranceDuration)
    {
//        dd(!isset($websiteName));
        if(!isset($websiteName))
        {
//            dd(!isset($websiteName));
            $m="Enter Your WebSite Name ";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 0;
        }

        if(!isset($normalUserLoad))
        {
            $m="Enter Normal Load (users) Expected ";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 0;
        }

        if(!isset($normalLoadDuration))
        {
            $m="Enter Duration For Normal Load (users) Expected ";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 0;
        }

        if(!isset($peakUserLoad))
        {
            $m="Enter Peak Load (users) Expected";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 0;
        }
        if(!isset($peakLoadDuration))
        {
            $m="Enter Duration For Peak Load (users) Expected ";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 0;
        }
        if(!isset($futureUserLoad))
        {
            $m="Enter Future Load (users) Expected ";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 0;
        }
        if(!isset($futureLoadDuration))
        {
            $m="Enter Duration For Future Load (users) Expected ";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 0;
        }
        if(!isset($enduranceDuration))
        {
            $m="Enter The longest expected time can be maintained on the load continuously";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 0;
        }
        return 1;
    }


    public function saveUserLoadConfig()
    {

        $r=$this->validateUserLoad($this->websiteName,$this->normalUserLoad,$this->normalLoadDuration,
            $this->peakUserLoad,$this->peakLoadDuration,$this->futureUserLoad,$this->futureLoadDuration,$this->enduranceDuration);
//dd($r);
        if($r==0)
        {
//            dd('dd');
            return;
        }
//        dd('dd');
        $numberOfUsersInEndurance=$this->normalUserLoad*$this->enduranceDuration*60/$this->normalLoadDuration;
        $load['websiteName']=$this->websiteName;
        $load['normal']['user']=$this->normalUserLoad;
        $load['normal']['duration']=$this->normalLoadDuration;
        $load['peak']['user']=$this->peakUserLoad;
        $load['peak']['duration']=$this->peakLoadDuration;
        $load['future']['user']=$this->futureUserLoad;
        $load['future']['duration']=$this->futureLoadDuration;
        $load['endurance']['user']=$numberOfUsersInEndurance;
        $load['endurance']['duration']=$this->enduranceDuration*60;
//        dd($load);
        $this->storeUserLoad($load);
        $m="Saved Successfully";
        $this->emit('alert',['icon'=>'success','title'=>$m]);

    }
}
