<?php

namespace App\Traits;
trait FourthStep
{
    public function SaveIterationConfig($iteration,$iterationNumInTable)
    {
        if(!isset($this->pacing[$iteration]))
        {
            $m="Enter The Pacing Time For Iteration ".$iterationNumInTable;
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return;
        }
        if(!isset($this->iterationLoop[$iteration]))
        {
            $m="Enter The Loop Count  For Iteration ".$iterationNumInTable;
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return;
        }
        $this->selectedIterations[$iteration]['pacing']=$this->pacing[$iteration];
        $this->selectedIterations[$iteration]['loopCount']=$this->iterationLoop[$iteration];
        $this->storeStoredIterations($this->selectedIterations);
        $m="Saved Successfully";
        $this->emit('alert',['icon'=>'success','title'=>$m]);

//        dd($this->pacing[$iteration],$this->iterationLoop[$iteration], $this->selectedIterations);
    }


}
