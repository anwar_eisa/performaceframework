<?php

namespace App\Traits;

trait IterationTrait
{

    //for GeneratedIterations
    public function getAllGeneratedIterations()
    {
        return request()->session()->get('generatedIterations');
    }

    public function storeGeneratedIterations($generatedIteration)
    {
//        dd($generatedIteration);
        $old = $this->getAllGeneratedIterations();
        if($old==null)
        {
            $this->clearAllGeneratedIterations();
            $old = $this->getAllGeneratedIterations();
        }
//        dd($old);
        //check if path exist
        if (count($old) != 0) {
            foreach ($old as $oldPath) {
                if ($oldPath === $generatedIteration) {
                    return;
                }
            }
            $this->clearAllGeneratedIterations();
            array_push($old, $generatedIteration);
            request()->session()->put('generatedIterations', $old);

        } else {
            array_push($old, $generatedIteration);
            request()->session()->put('generatedIterations', $old);
        }

    }


    public function clearAllGeneratedIterations()
    {
        $newIteration = [];
        request()->session()->put('generatedIterations', $newIteration);

    }

    //for StoredIterations
    public function getAllStoredIterations()
    {
        return request()->session()->get('StoredIterations');
    }

    public function storeStoredIterations($storedIteration)
    {
        $this->clearAllStoredIterations();
        request()->session()->put('StoredIterations', $storedIteration);
    }


    public function clearAllStoredIterations()
    {
        $newIteration = [];
        request()->session()->put('StoredIterations', $newIteration);

    }

    //for sorted pages
    // key of array is old value and value of array is new value
    public function getAllPagesChanged()
    {
        return request()->session()->get('PagesChanged');
    }

    public function storePagesChanged($pages)
    {
        $this->clearAllPagesChanged();
        request()->session()->put('PagesChanged', $pages);
    }


    public function clearAllPagesChanged()
    {
        $pages = [];
        request()->session()->put('PagesChanged', $pages);

    }
}
