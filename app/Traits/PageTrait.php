<?php

namespace App\Traits;
trait PageTrait{
    public function getAllPages()
    {
        return request()->session()->get('pages');
    }

    public function store($newPages)
    {
        $this->clearAllPages();
        request()->session()->put('pages',$newPages);
    }
    public function saveAllPages($pages,$responseTime,$thinkTime)
    {
        $newPages=$this->getAllPages();
//        dd($newPages,$pages,$responseTime,$thinkTime);
//        $new['id']=$pages[]
        foreach ($pages as $key=>$p)
        {
            $new['id']=$key;
            $new['name']=$p;
            $new['responseTime']=$responseTime[$key];
            $new['thinkTime']=$thinkTime[$key];

            $newPages[$key]=$new;
        }
//        array_push($newPages,$new);
//        dd($newPages);
        $this->store($newPages);
    }

    public function clearAllPages()
    {
        $newPages=[];
        request()->session()->put('pages',$newPages);

    }
}
