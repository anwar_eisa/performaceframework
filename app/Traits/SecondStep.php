<?php

namespace App\Traits;
trait SecondStep
{

    public function submitIn($key)
    {
//        $allPages=$this->getAllPages();
        $this->allPagesInSecond=$this->getAllPages();
        $in = array();
        foreach ($this->pageIN as $key1=>$value)
        {
            if($value==false)
            {
                unset($this->pageIN[$key1]);
            }
            else
            {
                $in[]=$key1;
            }
//            $new[]=$value;
        }
//        $in= json_encode(array_keys($this->pageIN));
//        $in=$this->pageIN;
        $this->allPagesInSecond[$key]['in']=$in;
        $this->store($this->allPagesInSecond);
        $this->savedIn=true;
        $m="Saved Successfully";
        $this->emit('alert',['icon'=>'success','title'=>$m]);
//        dd($this->pagesName,$key,$this->pageIN,$in,$this->allPagesInSecond);
    }
    public function submitOut($key)
    {
        $this->allPagesInSecond=$this->getAllPages();
        $out = array();
        foreach ($this->pageOut as $key1=>$value)
        {
            if($value==false)
            {
                unset($this->pageOut[$key1]);
            }
            else
            {
                $out[]=$key1;
            }
//            $new[]=$value;
        }
//        $out= json_encode(array_keys($this->pageOut));
        $this->allPagesInSecond[$key]['out']=$out;
        $this->store($this->allPagesInSecond);
        $this->savedOut=true;
        $m="Saved Successfully";
        $this->emit('alert',['icon'=>'success','title'=>$m]);
//        dd($this->pagesName,$key,$this->pageOut,$out,$this->allPagesInSecond);
    }
    public function checkForPrevious()
    {
        if(!isset($this->allPagesInSecond[ $this->firstPageInSecond]))
        {
            $this->firstPageInSecond=$this->firstPageInSecond-1;
            $this->checkForPrevious();

        }
    }
    public function previousPageNavigate()
    {
        $this->pageOut=[];
        $this->pageIN=[];
        $this->savedIn=false;
        $this->savedOut=false;
        $this->firstPageInSecond=$this->firstPageInSecond-1;
        $this->checkForPrevious();

//        dd($this->firstPageInSecond,$this->allPagesInSecond);

//        dd($this->allPagesInSecond[ $this->firstPageInSecond]['out'],$this->pageOut);
        if(isset($this->allPagesInSecond[ $this->firstPageInSecond]['out']))
        {
//            $this->pageOut=$this->allPagesInSecond[ $this->firstPageInSecond]['out'];
            foreach ($this->allPagesInSecond[ $this->firstPageInSecond]['out'] as $key=>$page)
            {
                $this->pageOut[$page]  =$page;
            }
            $this->savedOut=true;
        }
        if(isset($this->allPagesInSecond[ $this->firstPageInSecond]['in']))
        {
//            $this->pageIN=$this->allPagesInSecond[ $this->firstPageInSecond]['in'];
            foreach ($this->allPagesInSecond[ $this->firstPageInSecond]['in'] as $key=>$page)
            {
                $this->pageIN[$page]  =$page;
            }
            $this->savedIn=true;
        }
    }
    public function checkForNext()
    {
        if(!isset($this->allPagesInSecond[ $this->firstPageInSecond]))
        {
            $this->firstPageInSecond=$this->firstPageInSecond+1;
            $this->checkForNext();
        }
    }
    public function nextPageNavigate()
    {
//        dd('dd');
//        dd($this->pageIN);
        $this->pageIN=[];
        $this->pageOut=[];
        $this->savedIn=false;
        $this->savedOut=false;
        $this->firstPageInSecond=$this->firstPageInSecond+1;
        $this->checkForNext();

        if(isset($this->allPagesInSecond[ $this->firstPageInSecond]['in']))
        {
//            $this->pageIN=$this->allPagesInSecond[ $this->firstPageInSecond]['in'];
            foreach ($this->allPagesInSecond[ $this->firstPageInSecond]['in'] as $key=>$page)
            {
                $this->pageIN[$page]  =$page;
            }
            $this->savedIn=true;
        }

        if(isset($this->allPagesInSecond[ $this->firstPageInSecond]['out']))
        {
//            $this->pageOut=$this->allPagesInSecond[ $this->firstPageInSecond]['out'];
            foreach ($this->allPagesInSecond[ $this->firstPageInSecond]['out'] as $key=>$page)
            {
                $this->pageOut[$page]  =$page;
            }
            $this->savedOut=true;
        }
//        dd($this->allPagesInSecond[ $this->firstPageInSecond], $this->pageIN, $this->pageOut);
//        dd($this->allPagesInSecond[ $this->firstPageInSecond],$this->allPagesInSecond,$this->allPagesInSecond);
    }

}
