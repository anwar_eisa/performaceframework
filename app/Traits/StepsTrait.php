<?php

namespace App\Traits;
use App\Models\PageJmeterResult;
use App\Models\PageResult;
use App\Models\TestDetails;
use App\Models\WebPages;
use Illuminate\Support\Arr;
trait StepsTrait
{
    public function startMyTest()
    {
//        $this->clearAllPages();
//        $this->clearAllGeneratedIterations();
//        $this->clearAllStoredIterations();
        $this->allPagesInFirst=[];
        $this->pages=[1];
        $this->i=1;
        $this->firstStep=true;
        $this->zeroStep=false;
        $this->thirdStep=false;
        $this->secondStep=false;
        $this->fourthStep=false;
        $this->sixthStep=false;
        $this->seventhStep=false;

        $this->allPagesInFirst=$this->getAllPages();
        if(isset($this->allPagesInFirst) and count($this->allPagesInFirst)!=0)
        {
            $this->i=count($this->allPagesInFirst)+1;
            for ($i=2 ;$i<=$this->i; $i++)
            {
                array_push($this->pages,$i);
            }
        }
        $load=$this->getAllUserLoad();
//        dd($load);

        if(isset($load['normal']['user']))
        {
            $this->websiteName=$load['websiteName'];
            $this->normalUserLoad=$load['normal']['user'];
            $this->normalLoadDuration=$load['normal']['duration'];
            $this->peakUserLoad=$load['peak']['user'];
            $this->peakLoadDuration= $load['peak']['duration'];
            $this->futureUserLoad=$load['future']['user'];
            $this->futureLoadDuration=$load['future']['duration'];
        }
//        dd($this->pagesName);

//        dd( $this->allPagesInFirst,$this->pages,$this->i);

    }
    public function startSecondPage()
    {
        $this->loadInfo=$this->getAllUserLoad();
//        dd($this->pagesName,$this->iterationsInFifth,$this->iterationsGenerated,$this->selectedIterations);
//        dd(count($this->loadInfo),$this->loadInfo);
        if(count($this->loadInfo)==6 or count($this->loadInfo)==5 )
        {
            $result= $this->validateUserLoad($this->loadInfo['websiteName'],$this->loadInfo['normal']['user'],$this->loadInfo['normal']['duration'],
                $this->loadInfo['peak']['user'],$this->loadInfo['peak']['duration'],
                $this->loadInfo['future']['user'],$this->loadInfo['future']['duration'],
                $this->loadInfo['endurance']['duration']);

            if($result==0)
            {
                return;
            }
        }
        else
        {
            $m="Enter  All WebSite Details ";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return;
        }


        $this->allPagesInSecond=$this->getAllPages();
//        dd($this->allPagesInSecond);
        $this->firstKeyInSecond = array_key_first($this->allPagesInSecond);
        $this->lastKeyInSecond = array_key_last($this->allPagesInSecond);

        $this->firstPageInSecond=$this->allPagesInSecond[ $this->firstKeyInSecond]['id'];
        if(count($this->allPagesInSecond)<1)
        {
            $m="Please Enter one Page at Least";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return;
        }
//        dd(count($newPages),$newPages);
        $this->secondStep=true;
        $this->firstStep=false;
        $this->zeroStep=false;
        $this->thirdStep=false;
        $this->fourthStep=false;
        $this->fifthStep=false;
        $this->sixthStep=false;
        $this->seventhStep=false;

        // if there is old values
        if(isset($this->allPagesInSecond[ $this->firstPageInSecond]['in']))
        {
//            $this->pageIN=$this->allPagesInSecond[ $this->firstPageInSecond]['in'];
            foreach ($this->allPagesInSecond[ $this->firstPageInSecond]['in'] as $key=>$page)
            {
                $this->pageIN[$page]  =$page;
//                array_push($this->pageIN,$page);

            }
            $this->savedIn=true;
        }
        else
        {
            $this->savedIn=false;
            $this->pageIN  =[];

        }

//        dd($this->allPagesInSecond[ $this->firstPageInSecond]['out'],$this->allPagesInSecond[ $this->firstPageInSecond],$this->allPagesInSecond);
        if(isset($this->allPagesInSecond[ $this->firstPageInSecond]['out']))
        {
//            $this->pageOut=$this->allPagesInSecond[ $this->firstPageInSecond]['out'];
            $this->pageOut=[];
            foreach ($this->allPagesInSecond[ $this->firstPageInSecond]['out'] as $key=>$page)
            {
                $this->pageOut[$page]  =$page;
//                array_push($this->pageOut,$page);
            }
//                    dd($this->allPagesInSecond[ $this->firstPageInSecond]['out'], $this->pageOut,$this->allPagesInSecond[ $this->firstPageInSecond],$this->allPagesInSecond);

            $this->savedOut=true;
        }
        else
        {
            $this->savedOut=false;
            $this->pageOut  =[];

        }

    }
    public function goToThirdStep()
    {
        $dataSource=$this->getAllPages();
//        dd($dataSource);
//        $this->clearAllStoredIterations();
//        $this->clearAllGeneratedIterations();
//        dd('ddd',$this->getAllGeneratedIterations(),$this->getAllStoredIterations(),$this->selectedIterations,$this->getAllStoredIterations());
        foreach ($dataSource as $key=>$data)
        {
            if(!isset($dataSource[$key]['in']))
            {
                $m="Enter The Pages That Refer to this Page (In TO) ".$dataSource[$key]['name'];
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return;
            }
            if(!isset($dataSource[$key]['out']))
            {
                $m="Enter the Pages to which this page Refers (Out Of) ".$dataSource[$key]['name'];
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return;
            }
        }
        $this->secondStep=false;
        $this->firstStep=false;
        $this->zeroStep=false;
        $this->fourthStep=false;
        $this->fifthStep=false;
        $this->thirdStep=true;
        $this->sixthStep=false;
        $this->seventhStep=false;
//        dd( $this->dataSource);

        $this->getRankOfAllNodes();
        $this->dataSource=$this->getAllPages();
//        dd($this->dataSource);
//        $rank = array_column( $this->dataSource, 'rank');
//        array_multisort($rank, SORT_DESC,  $this->dataSource);
//        $this->dataSource = collect($this->dataSource)->sortByDesc('rank')->toArray();

    }

    public function goToFourthStep()
    {
        $this->secondStep=false;
        $this->firstStep=false;
        $this->zeroStep=false;
        $this->thirdStep=false;
        $this->fourthStep=true;
        $this->fifthStep=false;
        $this->sixthStep=false;
        $this->seventhStep=false;
        $this->selectedIterations=$this->getAllStoredIterations();
        $this->dataSource=$this->getAllPages();

        foreach ($this->selectedIterations as $key=>$iteration)
        {
            if(isset($iteration['pacing']))
            {

                $this->pacing[$key]=$iteration['pacing'];
                $this->iterationLoop[$key]=$iteration['loopCount'];
            }
        }

//        dd($this->selectedIterations,$this->getAllGeneratedIterations(),$this->dataSource);
    }
    public function goToFifthStep()
    {

//        dd($this->loadInfo['normal']['user']);

        //validate Fourth Step Entries
        $storedIteration=$this->getAllStoredIterations();
        $i=1;
        foreach ($storedIteration as $key=>$iteration)
        {
            if(!isset($iteration['pacing']))
            {
                $m="Enter The Pacing Time For Iteration ".$i;
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return;
            }
            if(!isset($iteration['loopCount']))
            {
                $m="Enter The Loop Count  For Iteration ".$i;
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return;
            }
            $i++;
        }


        $this->getUsersForIterations();

        $this->loadInfo=$this->getAllUserLoad();

        $this->testName= "Simulate User Experience";
        $this->website=$this->loadInfo['websiteName'];
        $this->iterationsInFifth=$this->getAllStoredIterations();
        $this->pagesInFifth=$this->getAllPages();

//        $normalUser='normalUser';
//        $rampUpTime=$this->loadInfo['normal']['duration']*60;
//        $expectedThroughputInNormal= $this->getExpectedThroughputForPages($this->pagesInFifth,$this->iterationsInFifth,$normalUser,$rampUpTime);

//        $peakUser='peakUser';
//        $rampUpTime=$this->loadInfo['peak']['duration']*60;
//        $expectedThroughputInPeak= $this->getExpectedThroughputForPages($this->pagesInFifth,$this->iterationsInFifth,$peakUser,$rampUpTime);

//        $futureUser='futureUser';
//        $rampUpTime=$this->loadInfo['future']['duration']*60;
//        $expectedThroughputInFuture= $this->getExpectedThroughputForPages($this->pagesInFifth,$this->iterationsInFifth,$futureUser,$rampUpTime);

        if(!isset($this->loadInfo['testID']) )
        {
            $this->testID=time().rand(1000000,100000000000);
            $this->loadInfo['testID']=$this->testID;
            $this->storeUserLoad($this->loadInfo);
        }
        else
        {
       $this->testID=$this->loadInfo['testID'];
        }
        $storedTestID=TestDetails::where('testID',$this->testID)->first();
        if($storedTestID==null)
        {
            $newTest=TestDetails::create([
                'testID'=>$this->testID,
                'websiteName'=>$this->website,
            ]);
            foreach ($this->pagesInFifth as $page)
            {
                WebPages::create([
                    'pageName'=> $page['name'],
                    'responseTime'=> $page['responseTime'],
                    'testID'=> $newTest->id,
                ]);
            }

        }

//        dd($this->loadInfo,$this->testID, $this->pagesInFifth,$storedTestID);

//        dd($this->iterationsInFifth);
        $this->secondStep=false;
        $this->firstStep=false;
        $this->zeroStep=false;
        $this->thirdStep=false;
        $this->fourthStep=false;
        $this->fifthStep=true;
        $this->sixthStep=false;
        $this->seventhStep=false;
        $normalUser=null;
        $peakUser=null;
        $futureUser=null;
        $rampUpTime=null;
        $expectedThroughputInNormal=null;
        $expectedThroughputInPeak=null;
        $expectedThroughputInFuture=null;
        $newTest=null;
    }

    public function goToSixStep()
    {

        $this->secondStep=false;
        $this->firstStep=false;
        $this->zeroStep=false;
        $this->thirdStep=false;
        $this->fourthStep=false;
        $this->fifthStep=false;
        $this->sixthStep=true;
        $this->seventhStep=false;

//        dd($this->resultsFromJTMitrex,$this->resultsFromJMeter);
    }

    public function goToSeventhStep()
    {
        $this->secondStep=false;
        $this->firstStep=false;
        $this->zeroStep=false;
        $this->thirdStep=false;
        $this->fourthStep=false;
        $this->fifthStep=false;
        $this->sixthStep=false;
        $this->seventhStep=true;
        $this->testInfo=TestDetails::where('testID',$this->testIDReporting)
            ->first();
        $this->websiteNameReporting=$this->testInfo->websiteName;
        $this->storedResults=WebPages::with(['pageResults','pageJMeterResults','pageImprovement'])
            ->where('testID',$this->testInfo->id)
            ->get()->toArray();

//        dd($this->testInfo);
    }

    public function getGTMetricsResult($pageID)
    {
        $page = Arr::where($this->storedResultsGTMetrics, function($value,$key) use ($pageID)
        {
//            dd($value,$key,$value['pageID']);
            return $value['pageID']==$pageID;
        });
        return $page;
    }

}
