<?php

namespace App\Traits;
use Illuminate\Support\Arr;

trait ThirdStep
{

    public function generateIterations()
    {
//        $this->clearAllGeneratedIterations();
//        dd($this->getAllGeneratedIterations());
//        $this->resortDataSourceAfterDeletePage();
//        foreach ($this->dataSource as $arrayPage)
//        {
//            collect($arrayPage)
//        }
//        $this->dataSource = collect($this->dataSource)->sortByDesc('rank');

//        $this->dataSource=$this->getAllPages();
//        $pageSorted=$this->resortDataSourceAfterDeletePage($this->dataSource);

//        dd($this->dataSource,$this->firstNode,$this->endNode,$pageSorted);
//        $this->showPath=true;
//        dd($this->dataSource,$this->firstNode,$this->endNode);
        if($this->firstNode==null)
        {
            $m="Please Select Start Node ";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return;
        }
        if($this->endNode==null)
        {
            $m="Please Select End Node ";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return;
        }
        if($this->throughPages==null)
        {
            $m="Please Select Through Pages ";
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return;
        }
        if(!in_array($this->firstNode,$this->throughPages))
        {
            array_push($this->throughPages,$this->firstNode);
        }
        if(!in_array($this->endNode,$this->throughPages))
        {
            array_push($this->throughPages,$this->endNode);
        }
        sort($this->throughPages);
        foreach ($this->throughPages as $page)
        {
            $pageGroup[$page]=$this->dataSource[$page];
            $in=[];
            $out=[];
            foreach ($pageGroup[$page]['in'] as $inPage)
            {
                if(in_array($inPage,$this->throughPages))
                {
                    array_push($in,$inPage);
                }
            }
            $pageGroup[$page]['in']=$in;

            foreach ($pageGroup[$page]['out'] as $outPage)
            {
                if(in_array($outPage,$this->throughPages))
                {
                    array_push($out,$outPage);
                }
            }
            $pageGroup[$page]['out']=$out;

        }
//        dd($pageGroup);

        $pageGroupSorted=$this->sortPageGroup($pageGroup);
        $pagesChanges=$this->getAllPagesChanged();

        $old=array_keys($pagesChanges);
//        dd($old,$pagesChanges);
//        $new=array_values($pagesChanges);

        if(in_array($this->firstNode,$old))
        {
           $nodeFirst=$pagesChanges[$this->firstNode];
        }
        else
        {
            $nodeFirst=$this->firstNode;
        }
        if(in_array($this->endNode,$old))
        {
           $nodeEnd=$pagesChanges[$this->endNode];
        }
        else
        {
            $nodeEnd=$this->endNode;
        }

//        $this->clearAllGeneratedIterations();

//            dd($this->throughPages,$this->firstNode,$this->endNode,$pageGroup,$pageGroupSorted);
//        $this->endNode=5;

        $this->dispatchBrowserEvent('test', [
            'nods' => $pageGroupSorted,
//            'nods' => $this->getDataSource(),
            'start'=>$nodeFirst,
            'end'=>$nodeEnd]);
//        dd("jj");
    }

    public function sortPageGroup($pageGroup)
    {
        $newPages=[];
        $changes=[];
        $old=[];
//        $new=[];
        $id=1;
        foreach ($pageGroup as $page)
        {
            if($id!=$page['id'])
            {
                $changes[$page['id']]=$id;
                array_push($old,$page['id']);
                $page['id']=$id;
            }
            $newPages[$id]=$page;
            $id++;
        }

        foreach ($newPages as $k=>$page)
        {
            foreach ($old as $d)
            {
                if(in_array($d,$page['in']))
                {
                    foreach ($page['in'] as $key=>$in)
                    {
                        if($d==$in)
                        {
                            $page['in'][$key]= $changes[$d];
                        }
                    }
//                    dd($page['in']);
                }

                if(in_array($d,$page['out']))
                {
                    foreach ($page['out'] as $key=>$out)
                    {
                        if($d==$out)
                        {
                            $page['out'][$key]= $changes[$d];
                        }
                    }
//                    dd($page['out']);
                }
            }
            $newPages[$k]=$page;
        }
        $this->storePagesChanged($changes);
//        dd($newPages,$changes,$old);

        return $newPages;



    }
    public function getLastNodeNameProperty()
    {
//        dd($this->dataSource);
        $this->pathDiscoverResult=false;
        $this->iterationsGenerated=[];
        return Arr::where($this->dataSource, function ($value, $key) {
            return $value['id']=$this->endNode;
        });
    }

    public function getFirstNodeNameProperty()
    {
        $this->pathDiscoverResult=false;
        $this->iterationsGenerated=[];
//        dd($this->firstNode,$this->dataSource);
        return Arr::where($this->dataSource, function ($value, $key) {
            return $value['id']=$this->firstNode;
        });
    }
    public function sortDataSourceByRank($dataSource)
    {
        $rank = array_column( $dataSource, 'rank');
        array_multisort($rank, SORT_DESC,  $dataSource);
        foreach ($dataSource as $array)
        {
//            dd($array);
            $id=$array['id'];
            $orderedData[$id]= $array;
        }
        return $orderedData;
    }
    public function newPathDiscovered($path)
    {
//        dd($path);
        $path=json_decode($path);
        $pagesChanges=$this->getAllPagesChanged();
        $new=array_values($pagesChanges);
        foreach ($pagesChanges as $k=>$v)
        {
            $pagesChangesReversed[$v]=$k;
        }
        foreach ($new as $n)
        {
            if(in_array($n,$path))
            {
                foreach ($path as $k=>$v)
                {
//                    dd($v,$n);
                    if($v==$n)
                    {
//                        dd($pagesChangesReversed[$n]);
                        $path[$k]=$pagesChangesReversed[$n];
                    }
                }
            }
        }
//        dd($path,$pagesChanges,$pagesChangesReversed,$new,'ll');
        $this->storeGeneratedIterations($path);
        $this->iterationsGenerated=$this->getAllGeneratedIterations();
        $this->selectedIterations=$this->getAllStoredIterations();
        $this->iterationsGenerated=$this->filterIterationsByNodes($this->iterationsGenerated,$this->firstNode,$this->endNode);
        $this->pathDiscoverResult=true;
//    dd($this->dataSource);
    }
    public function filterIterationsByNodes($iterations,$firstNode,$endNode)
    {
        foreach ($iterations as $key=>$path)
        {
            if($path[array_key_first($path)]!=$firstNode or $path[array_key_last($path)]!=$endNode)
            {
                unset($iterations[$key]);
            }
        }
        return $iterations;
    }
    public function saveIterations($key)
    {
//        $selectedIterations=[];
//        dd($key);
        $this->selectedIterations=$this->getAllStoredIterations();
//        dd($this->selectedIterations);
        if(isset($this->selectedIterations[$key]))
        {
//            dd('delete');
            unset($this->selectedIterations[$key]);
            $this->storeStoredIterations($this->selectedIterations);
        }
        else
        {
//            dd('add');

            $this->iterationsGenerated=$this->getAllGeneratedIterations();
            $this->selectedIterations[$key]['pages']=$this->iterationsGenerated[$key];
//            dd($this->selectedIterations);
            $this->storeStoredIterations($this->selectedIterations);
        }
        $this->selectedIterations=$this->getAllStoredIterations();
        $this->iterationsGenerated=$this->getAllGeneratedIterations();
        $this->iterationsGenerated=$this->filterIterationsByNodes($this->iterationsGenerated,$this->firstNode,$this->endNode);


    }

}
