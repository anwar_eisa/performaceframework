<?php

namespace App\Traits;

trait UserLoadTrait
{


    //for User Load
    public function getAllUserLoad()
    {
        return request()->session()->get('UserLoad');
    }

    public function storeUserLoad($userLoad)
    {
        $this->clearAllUserLoad();
        request()->session()->put('UserLoad', $userLoad);
    }


    public function clearAllUserLoad()
    {
      $userLoad = [];
        request()->session()->put('UserLoad',$userLoad);

    }
}
