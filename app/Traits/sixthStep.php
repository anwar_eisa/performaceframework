<?php

namespace App\Traits;


use App\Models\PageImprovements;
use App\Models\PageJmeterResult;
use App\Models\PageResult;
use App\Models\TestDetails;
use App\Models\WebPages;
use Illuminate\Support\Facades\DB;

trait sixthStep
{



    public function getAllGtmetricsResults()
    {

//        $this->addedGTMitrics=PageResult::with('webPage')
//            ->where('testID',$this->selectedTestID)
//            ->get()
//            ->groupBy('pageID')
//        ->toArray();
        $this->addedGTMitrics=PageResult::with('webPage')
        ->select('pageID')->distinct('pageID')->get()->toArray();

//        dd($this->addedGTMitrics);
    }
    public function getAllJmeterResults()
    {
        $this->addedJmeters=PageJmeterResult::with('webPage')->orderBy('pageID','ASC')
            ->where('testID',$this->selectedTestID)
            ->get()->toArray();
//        dd($this->addedJmeters,count($this->addedJmeters));

    }

    public function deleteGTMetricsResult($pageId)
    {
//        dd($pageId);
        $v=PageResult::where('pageID',$pageId)->get();
        foreach ($v as $c)
        {
            $c->delete();
        }
        $v=null;
        $this->getAllGtmetricsResults();
    }
    public function deleteJMeterResult($pageId)
    {
//        dd($pageId);
        $r=PageJmeterResult::find($pageId);
        $r->delete();
        $r=null;
        $this->getAllJmeterResults();
    }

    public function getStandardDeviation($values,$average){

        $standardDeviation=null;
        $squareOfDistance=[];
        foreach ($values as $value){
           $new= $value-$average;
           $new=$new*$new;
           array_push($squareOfDistance,$new);
        }
        $varience=array_sum($squareOfDistance)/(count($values)-1);
        $standardDeviation=sqrt($varience);
        $squareOfDistance=[];
        $average=null;
        return $standardDeviation;
    }

   public function getAverage($array){
        return (array_sum($array)/count($array));
    }
    public function saveJMeterResult()
    {
//        $values=[3,2,15,1,4];
//        $values=[4,6,3,4,8];
//        $values=[5,5,6,4,5];
//        dd($this->storedPages);

//        $pageId=$this->storedPages[$this->jmeterResultCounter]['id'];
//        $expectedResponseTime=$this->storedPages[$this->jmeterResultCounter]['responseTime'];

//        dd($this->selectedPageResultJmeter,$expectedResponseTime);

//        dd($pageId);
        $this->validate([
            'minResponseTime' => 'required',
            'maxResponseTime' => 'required',
            'medianResponseTime' => 'required',
            'responseTime90th' => 'required',
            'responseTime95th' => 'required',
            'responseTime99th' => 'required',
            'errorRatio' => 'required',
            'throughput' => 'required',
            'selectedScenario' => 'required',
            'selectedPageResultJmeter'=>'required',
        ]);
        $values=[$this->minResponseTime,$this->maxResponseTime,$this->medianResponseTime,
            $this->responseTime90th,$this->responseTime95th,$this->responseTime99th];
        $expectedResponseTime=WebPages::find($this->selectedPageResultJmeter)->responseTime;
        $average= $this->getAverage($values);
        $standardDeviation=$this->getStandardDeviation($values,$average);
        $standardDeviation=round($standardDeviation,2);
        $deviationFromExpectedResponseTime=$this->getStandardDeviation($values,$expectedResponseTime);
        $deviationFromExpectedResponseTime=round($deviationFromExpectedResponseTime,2);
        PageJmeterResult::create([
            'min' => $this->minResponseTime,
            'max' => $this->maxResponseTime,
            'median' => $this->medianResponseTime,
            '90th' => $this->responseTime90th,
            '95th' => $this->responseTime95th,
            '99th' => $this->responseTime99th,
            'error' => $this->errorRatio,
            'throughput' => $this->throughput,
            'scenario' => $this->selectedScenario,
            'deviation' => $standardDeviation,
            'deviation2' => $deviationFromExpectedResponseTime,
            'average' => $average,
            'pageID'=>$this->selectedPageResultJmeter,
            'testID'=>$this->selectedTestID,

        ]);
        $standardDeviation=null;
        $deviationFromExpectedResponseTime=null;
        $average=null;
        $this->resetJMeterPageResults();
        $this->getAllJmeterResults();
        $m="Saved Successfully";
        $this->emit('alert',['icon'=>'success','title'=>$m]);
//        dd($standardDeviation,$deviationFromExpectedResponseTime);
    }
    public function checkFromTestID()
    {
        $this->selectedTestID=null;
        $check=TestDetails::with('pages')
            ->where('testID',$this->testIDReporting)
            ->first();
//        dd($check->testID);
      if($check!=null)
      {
          $this->storedPages=$check->pages->toArray();
          $this->pageResultCounter=array_key_first($this->storedPages);
          $this->selectedTestID=$check->testID;
          $this->getAllJmeterResults();
          $this->getAllGtmetricsResults();
      }
      $this->resultsFromJTMitrex=true;
//        dd($this->testIDReporting,$check,$this->storedPages);
    }

    public function goToJMeterResults(){
        $this->resultsFromJTMitrex=false;
        $this->resultsFromJMeter=true;
    }
    public function goToGTMetrixResults()
    {
        $this->resultsFromJTMitrex=true;
        $this->resultsFromJMeter=false;
    }
    public function addNewImprovement($counter)
    {
        $counter++;
        array_push($this->improvementCounts,$counter);
//        dd($this->improvement);
    }
    public function savePageResult()
    {
//        dd($pageId);
        $this->validate([
            'GTPerformance' => 'required',
            'GTStructure' => 'required',
            'firstContentfulPaint' => 'required',
            'speedIndex' => 'required',
            'largestContentfulPaint' => 'required',
            'cumulativeLayoutShift' => 'required',
            'blockingTime' => 'required',
            'timeInteractive' => 'required',
            'firstContentfulPaintDes' => 'required',
            'speedIndexDes' => 'required',
            'largestContentfulPaintDes' => 'required',
            'cumulativeLayoutShiftDes' => 'required',
            'timeInteractiveDes' => 'required',
            'blockingTimeDes' => 'required',
            'selectedPageResultGTMitrics' => 'required',
        ]);
        $standards=['First Contentful Paint','Time to Interactive',
            'Speed Index','Total Blocking Time',
            'Largest Contentful Paint',
            'Cumulative Layout Shift',
            'Performance','Structure'
            ];
        $values=[$this->firstContentfulPaint,$this->timeInteractive,$this->speedIndex,
            $this->blockingTime,$this->largestContentfulPaint,$this->cumulativeLayoutShift,
            $this->GTPerformance,$this->GTStructure];
        $details=[$this->firstContentfulPaintDes,$this->timeInteractiveDes,$this->speedIndexDes,
            $this->blockingTimeDes,$this->largestContentfulPaintDes,$this->cumulativeLayoutShiftDes,
           null,null];
//        $pageId=$this->storedPages[$this->pageResultCounter]['id'];
        foreach ($standards as $key=>$standard)
        {
            PageResult::create([
                'standard'=>$standard,
                'value'=>$values[$key],
                'details'=>$details[$key],
                'pageID'=>$this->selectedPageResultGTMitrics,
                'testID'=>$this->selectedTestID,

            ]);
        }
        foreach ($this->improvement as $im)
        {
            PageImprovements::create([
                'improvements'=>$im,
                'pageID'=>$this->selectedPageResultGTMitrics,
            ]);
        }
        if(count($this->storedPages)!=$this->pageResultCounter){
            $this->pageResultCounter++;
            $this->resetPageResults();
//            dd(count($this->storedPages),$this->pageResultCounter);

        }
        else{
            dd(count($this->storedPages),$this->pageResultCounter);
            $m="You Have Finished All Pages ";
            $this->emit('alert',['icon'=>'success','title'=>$m]);
            return;
        }
        $m="Saved Successfully";
        $this->emit('alert',['icon'=>'success','title'=>$m]);
        $this->getAllGtmetricsResults();

//        dd($this->loadTime,$this->loadTimeDes);

    }

    public function resetJMeterPageResults()
    {
        $this->minResponseTime=null;
        $this->maxResponseTime=null;
        $this->medianResponseTime=null;
        $this->responseTime90th=null;
        $this->responseTime95th=null;
        $this->responseTime99th=null;
        $this->errorRatio=null;
        $this->throughput=null;
        $this->selectedScenario=null;
    }
    public function resetPageResults()
    {
        $this->GTStructure=null;
        $this->GTPerformance=null;
        $this->firstContentfulPaint=null;
        $this->speedIndex=null;
        $this->largestContentfulPaint=null;
        $this->cumulativeLayoutShift=null;
        $this->blockingTime=null;
        $this->timeInteractive=null;

        $this->loadTimeDes=null;
        $this->firstByteDEs=null;
        $this->startRenderDes=null;
        $this->firstContentfulPaintDes=null;
        $this->speedIndexDes=null;
        $this->largestContentfulPaintDes=null;
        $this->cumulativeLayoutShiftDes=null;
        $this->blockingTimeDes=null;
        $this->timeInteractiveDes=null;
        $this->improvement=[];
        $this->improvementCounts=[1];


    }

}
