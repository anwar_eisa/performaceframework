<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_results', function (Blueprint $table) {
            $table->id();
            $table->string('standard');
            $table->string('value');
            $table->string('details')->nullable();
            $table->unsignedBigInteger('pageID');
            $table->foreign('pageID')
                ->references('id')->on('web_pages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_results');
    }
}
