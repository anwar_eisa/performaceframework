<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageJmeterResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_jmeter_results', function (Blueprint $table) {
            $table->id();
            $table->string('scenario');
            $table->string('error');
            $table->string('min');
            $table->string('max');
            $table->string('median');
            $table->string('90th');
            $table->string('95th');
            $table->string('99th');
            $table->string('throughput');
            $table->unsignedBigInteger('pageID');
            $table->foreign('pageID')
                ->references('id')->on('web_pages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_jmeter_results');
    }
}
