<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeviationToPageJmeterResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_jmeter_results', function (Blueprint $table) {
            //
            $table->float('deviation');
            $table->float('deviation2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_jmeter_results', function (Blueprint $table) {
            //
        });
    }
}
