-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2022 at 05:27 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `preftester`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_01_10_220748_create_test_details_table', 1),
(6, '2022_01_10_221421_create_web_pages_table', 1),
(7, '2022_01_10_222538_create_page_results_table', 1),
(8, '2022_01_10_223915_create_page_jmeter_results_table', 1),
(9, '2022_01_11_210843_create_page_improvements_table', 1),
(11, '2022_01_15_111426_add_deviation_to_page_jmeter_results_table', 2),
(12, '2022_01_15_160614_add_average_to_page_jmeter_results_table', 2),
(15, '2022_01_15_192041_add_test_id_to_page_jmeter_results_table', 3),
(16, '2022_01_15_192118_add_test_id_to_page_results_table', 3),
(17, '2022_01_17_211310_add_website_name_to_test_details_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `page_improvements`
--

CREATE TABLE `page_improvements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `improvements` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pageID` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_improvements`
--

INSERT INTO `page_improvements` (`id`, `improvements`, `pageID`, `created_at`, `updated_at`) VALUES
(26, 'Enable text compression', 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07'),
(27, 'Use a Content Delivery Network (CDN)', 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07'),
(28, 'Minify CSS', 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07'),
(29, 'Minify JavaScript', 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07'),
(30, 'Enable text compression', 8, '2022-01-25 20:55:42', '2022-01-25 20:55:42'),
(31, 'Use a Content Delivery Network (CDN)', 8, '2022-01-25 20:55:42', '2022-01-25 20:55:42'),
(32, 'Minify CSS', 8, '2022-01-25 20:55:42', '2022-01-25 20:55:42'),
(33, 'Avoid large layout shifts', 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53'),
(34, 'Enable text compression', 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53'),
(35, 'Properly size images', 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53'),
(36, 'Use a Content Delivery Network (CDN)', 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53'),
(37, 'Enable text compression', 10, '2022-01-25 21:17:54', '2022-01-25 21:17:54'),
(38, 'Properly size images', 10, '2022-01-25 21:17:54', '2022-01-25 21:17:54'),
(39, 'Use a Content Delivery Network (CDN)', 10, '2022-01-25 21:17:54', '2022-01-25 21:17:54'),
(40, 'Minify JavaScript', 10, '2022-01-25 21:17:54', '2022-01-25 21:17:54'),
(41, 'Enable text compression', 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54'),
(42, 'Properly size images', 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54'),
(43, 'Serve static assets with an efficient cache policy', 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54'),
(44, 'Minify JavaScript', 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54'),
(45, 'Enable text compression', 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49'),
(46, 'Serve static assets with an efficient cache policy', 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49'),
(47, 'Properly size images', 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49'),
(48, 'Use a Content Delivery Network (CDN)', 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49'),
(49, 'Use a Content Delivery Network (CDN)', 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05'),
(50, 'Properly size images', 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05'),
(51, 'Minify JavaScript', 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05'),
(52, 'Reduce unused CSS', 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05'),
(53, 'Enable text compression', 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48'),
(54, 'Properly size images', 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48'),
(55, 'Reduce unused CSS', 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48'),
(56, 'Use a Content Delivery Network (CDN)', 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48'),
(57, 'Enable text compression', 15, '2022-01-25 21:35:56', '2022-01-25 21:35:56'),
(58, 'Use a Content Delivery Network (CDN)', 15, '2022-01-25 21:35:56', '2022-01-25 21:35:56'),
(59, 'Enable text compression', 16, '2022-01-25 21:37:42', '2022-01-25 21:37:42'),
(60, 'Use a Content Delivery Network (CDN)', 16, '2022-01-25 21:37:42', '2022-01-25 21:37:42'),
(61, 'Enable text compression', 17, '2022-01-25 21:39:42', '2022-01-25 21:39:42'),
(62, 'Eliminate render-blocking resources', 17, '2022-01-25 21:39:42', '2022-01-25 21:39:42'),
(63, 'Enable text compression', 18, '2022-01-25 21:43:46', '2022-01-25 21:43:46'),
(64, 'Use a Content Delivery Network (CDN)', 18, '2022-01-25 21:43:46', '2022-01-25 21:43:46');

-- --------------------------------------------------------

--
-- Table structure for table `page_jmeter_results`
--

CREATE TABLE `page_jmeter_results` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `scenario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `min` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `median` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `90th` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `95th` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `99th` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `throughput` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pageID` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deviation` double(8,2) NOT NULL,
  `deviation2` double(8,2) NOT NULL,
  `average` double(8,2) NOT NULL,
  `testID` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_jmeter_results`
--

INSERT INTO `page_jmeter_results` (`id`, `scenario`, `error`, `min`, `max`, `median`, `90th`, `95th`, `99th`, `throughput`, `pageID`, `created_at`, `updated_at`, `deviation`, `deviation2`, `average`, `testID`) VALUES
(19, 'Load Testing', '0.00%', '1.360', '9.664', '2.540', '8.394', '9.585', '9.663', '0.11', 7, '2022-02-05 16:48:09', '2022-02-05 16:48:09', 3.86, 4.37, 6.87, '16431506305079946651'),
(20, 'Load Testing', '0.00%', '1.194', '5.595', '2.244', '4.737', '5.243', '5.595', '0.06', 16, '2022-02-05 16:50:25', '2022-02-05 16:50:25', 1.90, 1.90, 4.10, '16431506305079946651'),
(21, 'Load Testing', '0.00%', '1.180', '5.720', '2.223', '4.853', '5.167', '5.720', '0.06', 17, '2022-02-05 16:51:52', '2022-02-05 16:51:52', 1.95, 1.96, 4.14, '16431506305079946651'),
(22, 'Load Testing', '0.00%', '1.411', '8.586', '4.420', '8.470', '8.561', '8.586', '	0.03', 18, '2022-02-05 16:54:45', '2022-02-05 16:54:45', 3.06, 3.57, 6.67, '16431506305079946651'),
(23, 'Load Testing', '0.00%', '1.394', '4.931', '1.677', '3.193', '3.254', '4.931', '0.06', 8, '2022-02-05 16:57:01', '2022-02-05 16:57:01', 1.52, 2.46, 3.23, '16431506305079946651'),
(24, 'Load Testing', '0.00%', '1.431', '4.873', '2.444', '3.558', '4.280', '4.873', '0.04', 9, '2022-02-05 16:58:26', '2022-02-05 16:58:26', 1.40, 2.09, 3.58, '16431506305079946651'),
(25, 'Load Testing', '0.00%', '1.324', '8.426', '4.433', '8.422', '8.426', '8.426', '0.01', 15, '2022-02-05 17:00:07', '2022-02-05 17:00:07', 3.03, 3.49, 6.58, '16431506305079946651'),
(26, 'Load Testing', '	0.00%', '1.323', '9.395', '1.502', '9.093', '9.395', '9.395', '0.01', 12, '2022-02-05 17:02:52', '2022-02-05 17:02:52', 4.09, 4.48, 6.68, '16431506305079946651'),
(27, 'Load Testing', '	0.00%	', '1.350', '8.976', '1.382', '8.784', '8.976', '8.976', '0.01', 14, '2022-02-05 17:15:08', '2022-02-05 17:15:08', 3.91, 4.20, 6.41, '16431506305079946651'),
(28, 'Load Testing', '0.00%', '1.286', '8.474', '4.421', '8.387', '8.469', '8.474', '	0.02', 11, '2022-02-05 17:16:57', '2022-02-05 17:16:57', 3.06, 3.51, 6.59, '16431506305079946651'),
(29, 'Load Testing', '0.00%', '1.248', '4.459', '3.420', '4.459', '4.459', '4.459', '0.02', 10, '2022-02-05 17:18:23', '2022-02-05 17:18:23', 1.29, 1.88, 3.75, '16431506305079946651'),
(30, 'Load Testing', '0.00%', '1.312', '8.392', '2.987', '8.380', '8.392', '8.392', '0.01', 13, '2022-02-05 17:20:09', '2022-02-05 17:20:09', 3.27, 3.57, 6.31, '16431506305079946651'),
(31, 'Peak Load Testing', '0.00%', '1.366', '23.215', '4.549', '9.307', '10.826', '15.705', '0.24', 7, '2022-02-05 17:27:31', '2022-02-05 17:27:31', 7.85, 10.12, 10.83, '16431506305079946651'),
(32, 'Peak Load Testing', '0.00%	', '1.181', '47.330', '2.860', '5.641', '8.400', '35.345', '0.13', 16, '2022-02-05 17:30:38', '2022-02-05 17:30:38', 19.54, 24.05, 16.79, '16431506305079946651'),
(33, 'Peak Load Testing', '0.00%', '1.185', '19.011', '3.121', '6.017', '10.308', '18.568', '0.13', 17, '2022-02-05 17:32:38', '2022-02-05 17:32:38', 7.68, 9.90, 9.70, '16431506305079946651'),
(34, 'Peak Load Testing', '0.00%', '1.405', '35.508', '46.66', '10.209', '12.738', '35.508', '0.06', 18, '2022-02-05 17:34:37', '2022-02-05 17:34:37', 17.92, 27.19, 23.67, '16431506305079946651'),
(35, 'Peak Load Testing', '0.00%', '1.394', '47.404', '2.321', '8.641', '9.928', '43.456', '0.11', 8, '2022-02-05 17:36:07', '2022-02-05 17:36:07', 20.89, 25.83, 18.86, '16431506305079946651'),
(36, 'Peak Load Testing', '0.00%', '1.405', '13.392', '2.085', '5.409', '7.139', '13.392', '0.08', 9, '2022-02-05 17:37:37', '2022-02-05 17:37:37', 5.28, 5.78, 7.14, '16431506305079946651'),
(37, 'Peak Load Testing', '0.00%', '1.441', '12.798', '4.182', '9.883', '12.355', '12.798', '	0.02', 15, '2022-02-05 17:41:05', '2022-02-05 17:41:05', 4.92, 6.53, 8.91, '16431506305079946651'),
(38, 'Peak Load Testing', '0.00%', '1.372', '16.224', '4.532', '5.344', '12.529', '16.224', '0.02', 12, '2022-02-05 17:42:30', '2022-02-05 17:42:30', 6.44, 8.03, 9.37, '16431506305079946651'),
(39, 'Peak Load Testing', '0.00%', '1.309', '23.261', '4.752', '11.881', '19.531', '23.261', '0.02', 14, '2022-02-05 17:44:11', '2022-02-05 17:44:11', 9.52, 13.70, 14.00, '16431506305079946651'),
(40, 'Peak Load Testing', '0.00%', '1.229', '16.926', '4.334', '8.810', '10.049', '16.926', '0.04', 11, '2022-02-05 17:46:01', '2022-02-05 17:46:01', 6.42, 8.24, 9.71, '16431506305079946651'),
(41, 'Peak Load Testing', '0.00%', '1.336', '13.283', '2.421', '6.379', '9.218', '13.283', '	0.03', 10, '2022-02-05 17:47:23', '2022-02-05 17:47:23', 5.19, 5.95, 7.65, '16431506305079946651'),
(42, 'Peak Load Testing', '0.00%', '1.259', '26.936', '2.800', '8.977', '21.754', '26.936', '0.02', 13, '2022-02-05 17:48:45', '2022-02-05 17:48:45', 11.87, 15.99, 14.78, '16431506305079946651'),
(43, 'Capacity Testing', '0.00%', '1.361', '27.014', '2.461', '7.366', '8.696', '11.226', '0.35', 7, '2022-02-05 18:04:18', '2022-02-05 18:04:18', 9.28, 10.60, 9.69, '16431506305079946651'),
(44, 'Capacity Testing', '0.00%', '1.196', '5.808', '2.487', '4.498', '4.736', '5.395', '	0.19', 16, '2022-02-05 18:05:51', '2022-02-05 18:05:51', 1.80, 1.80, 4.02, '16431506305079946651'),
(45, 'Capacity Testing', '0.00%', '1.185', '25.156', '2.337', '4.645', '5.211', '9.268', '0.20', 17, '2022-02-05 18:07:15', '2022-02-05 18:07:15', 8.87, 9.88, 7.97, '16431506305079946651'),
(46, 'Capacity Testing', '0.00%', '1.393', '28.675', '3.215', '8.595', '8.809', '28.675', '0.08', 18, '2022-02-05 18:08:37', '2022-02-05 18:08:37', 12.32, 15.26, 13.23, '16431506305079946651'),
(47, 'Capacity Testing', '0.00%', '1.388', '15.303', '1.717', '4.634', '5.134', '10.281', '0.15', 8, '2022-02-05 18:09:56', '2022-02-05 18:09:56', 5.41, 5.62, 6.41, '16431506305079946651'),
(48, 'Capacity Testing', '0.00%	', '1.445', '17.798', '2.466', '4.648', '6.770', '16.698', '	0.11', 9, '2022-02-05 18:11:17', '2022-02-05 18:11:17', 7.18, 8.04, 8.30, '16431506305079946651'),
(49, 'Capacity Testing', '0.00%', '1.258', '8.568', '2.772', '7.341', '8.478', '8.568', '0.03', 15, '2022-02-05 18:12:47', '2022-02-05 18:12:47', 3.28, 3.52, 6.16, '16431506305079946651'),
(50, 'Capacity Testing', '0.00%', '1.385', '9.612', '2.073', '6.996', '8.667', '9.612', '0.03', 12, '2022-02-05 18:14:02', '2022-02-05 18:14:02', 3.74, 4.04, 6.39, '16431506305079946651'),
(51, 'Capacity Testing', '0.00%', '1.233', '9.509', '1.951', '6.922', '8.554', '9.509', '0.03', 14, '2022-02-05 18:15:38', '2022-02-05 18:15:38', 3.76, 4.01, 6.28, '16431506305079946651'),
(52, 'Capacity Testing', '0.00%', '1.273', '14.651', '3.913', '8.423', '8.473', '14.651', '0.05', 11, '2022-02-05 18:18:58', '2022-02-05 18:18:58', 5.46, 6.71, 8.56, '16431506305079946651'),
(53, 'Capacity Testing', '0.00%', '1.279', '6.522', '2.905', '5.220', '6.222', '6.522', '0.04', 10, '2022-02-05 18:20:23', '2022-02-05 18:20:23', 2.20, 2.21, 4.78, '16431506305079946651'),
(54, 'Capacity Testing', '0.00%', '1.258', '15.604', '3.661', '8.364', '14.521', '15.604', '0.03', 13, '2022-02-05 18:21:39', '2022-02-05 18:21:39', 6.36, 8.28, 9.84, '16431506305079946651'),
(55, 'Spike Testing', '	0.00%', '1.377', '10.945', '2.978', '10.109', '10.748', '10.918', '0.31', 7, '2022-02-05 18:24:06', '2022-02-05 18:24:06', 4.43, 5.42, 7.85, '16431506305079946651'),
(56, 'Spike Testing', '0.00%', '1.204', '8.237', '2.267', '5.007', '5.435', '8.237', '0.24', 16, '2022-02-05 18:25:25', '2022-02-05 18:25:25', 2.93, 3.16, 5.06, '16431506305079946651'),
(57, 'Spike Testing', '0.00%', '1.179', '5.379', '2.029', '4.483', '5.063', '5.379', '	0.27', 17, '2022-02-05 18:26:44', '2022-02-05 18:26:44', 1.84, 1.84, 3.92, '16431506305079946651'),
(58, 'Spike Testing', '0.00%', '1.394', '9.303', '3.614', '8.498', '9.082', '9.303', '0.13', 18, '2022-02-05 18:28:02', '2022-02-05 18:28:02', 3.46, 4.02, 6.87, '16431506305079946651'),
(59, 'Spike Testing', '0.00%', '1.376', '9.491', '1.886', '3.276', '3.662', '9.491', '0.14', 8, '2022-02-05 18:29:53', '2022-02-05 18:29:53', 3.68, 3.69, 4.86, '16431506305079946651'),
(60, 'Spike Testing', '0.00%', '1.437', '5.032', '2.014', '3.798', '4.581', '5.032', '	0.10', 9, '2022-02-05 18:31:21', '2022-02-05 18:31:21', 1.57, 2.16, 3.65, '16431506305079946651'),
(61, 'Spike Testing', '0.00%	', '1.359', '9.439', '2.441', '9.111', '9.439', '9.439', '0.03', 15, '2022-02-05 18:32:45', '2022-02-05 18:32:45', 3.87, 4.38, 6.87, '16431506305079946651'),
(62, 'Spike Testing', '0.00%', '1.373', '8.767', '4.400', '8.651', '8.767', '8.767', '0.03', 12, '2022-02-05 18:34:06', '2022-02-05 18:34:06', 3.17, 3.73, 6.79, '16431506305079946651'),
(63, 'Spike Testing', '0.00%', '1.326', '8.340', '2.001', '7.428', '8.340', '8.340', '0.03', 14, '2022-02-05 18:35:26', '2022-02-05 18:35:26', 3.36, 3.52, 5.96, '16431506305079946651'),
(64, 'Spike Testing', '0.00%', '1.288', '9.931', '2.941', '8.820', '9.878', '9.931', '	0.05', 11, '2022-02-05 18:36:44', '2022-02-05 18:36:44', 3.94, 4.58, 7.13, '16431506305079946651'),
(65, 'Spike Testing', '0.00%', '1.251', '6.008', '1.508', '5.297', '6.008', '6.008', '	0.05', 10, '2022-02-05 18:38:01', '2022-02-05 18:38:01', 2.32, 2.42, 4.35, '16431506305079946651'),
(66, 'Spike Testing', '0.00%', '1.244', '8.530', '2.296', '7.321', '8.530', '8.530', '0.03', 13, '2022-02-05 18:39:23', '2022-02-05 18:39:23', 3.38, 3.58, 6.08, '16431506305079946651'),
(67, 'Endurance Testing', '0.00%', '1.353', '20.098', '2.740', '8.517', '9.041', '10.357', '0.16', 7, '2022-02-05 18:59:57', '2022-02-05 18:59:57', 6.67, 7.79, 8.68, '16431506305079946651'),
(68, 'Endurance Testing', '0.00%', '1.170', '17.344', '2.325', '4.809', '5.391', '8.354', '0.08', 16, '2022-02-05 19:01:47', '2022-02-05 19:01:47', 5.85, 6.49, 6.57, '16431506305079946651'),
(69, 'Endurance Testing', '0.00%', '1.174', '13.769', '2.414', '4.785', '5.534', '8.699', '0.07', 17, '2022-02-05 19:03:06', '2022-02-05 19:03:06', 4.59, 5.12, 6.06, '16431506305079946651'),
(70, 'Endurance Testing', '0.00%', '1.381', '18.149', '4.887', '8.957', '9.388', '14.506', '	0.03', 18, '2022-02-05 19:04:27', '2022-02-05 19:04:27', 6.12, 7.89, 9.54, '16431506305079946651'),
(71, 'Endurance Testing', '0.00%', '1.362', '16.307', '1.899', '4.089', '5.932', '9.412', '0.07', 8, '2022-02-05 19:07:14', '2022-02-05 19:07:14', 5.63, 5.86, 6.50, '16431506305079946651'),
(72, 'Endurance Testing', '0.00%', '1.405', '13.609', '2.142', '4.444', '5.259', '11.081', '	0.05', 9, '2022-02-05 19:08:45', '2022-02-05 19:08:45', 4.94, 5.15, 6.32, '16431506305079946651'),
(73, 'Endurance Testing', '0.00%', '1.314', '10.309', '4.642', '8.730', '9.130', '10.008', '0.01', 15, '2022-02-05 19:10:06', '2022-02-05 19:10:06', 3.60, 4.43, 7.36, '16431506305079946651'),
(74, 'Endurance Testing', '0.00%', '1.328', '9.744', '4.150', '8.397', '8.500', '9.505', '	0.01', 12, '2022-02-05 19:11:57', '2022-02-05 19:11:57', 3.41, 4.02, 6.94, '16431506305079946651'),
(75, 'Endurance Testing', '0.00%', '1.296', '10.009', '3.987', '8.388', '8.743', '9.891', '0.01', 14, '2022-02-05 19:13:14', '2022-02-05 19:13:14', 3.58, 4.22, 7.05, '16431506305079946651'),
(76, 'Endurance Testing', '0.00%', '1.267', '18.746', '4.460', '8.707', '9.062', '10.884', '0.02', 11, '2022-02-05 19:14:40', '2022-02-05 19:14:40', 5.98, 7.32, 8.85, '16431506305079946651'),
(77, 'Endurance Testing', '0.00%', '1.241', '12.535', '2.621', '5.065', '5.627', '7.122', '0.02', 10, '2022-02-05 19:16:05', '2022-02-05 19:16:05', 3.96, 4.04, 5.70, '16431506305079946651'),
(78, 'Endurance Testing', '	0.00%', '1.244', '17.321', '4.408', '8.414', '9.064', '15.248', '0.01', 13, '2022-02-05 19:17:31', '2022-02-05 19:17:31', 6.15, 7.74, 9.28, '16431506305079946651');

-- --------------------------------------------------------

--
-- Table structure for table `page_results`
--

CREATE TABLE `page_results` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `standard` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pageID` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `testID` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_results`
--

INSERT INTO `page_results` (`id`, `standard`, `value`, `details`, `pageID`, `created_at`, `updated_at`, `testID`) VALUES
(199, 'First Contentful Paint', '1.1s', 'OK, but consider improvement', 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07', '16431506305079946651'),
(200, 'Time to Interactive', '1.2s', 'Good - nothing to do here', 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07', '16431506305079946651'),
(201, 'Speed Index', '1.3s', 'Good - nothing to do here', 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07', '16431506305079946651'),
(202, 'Total Blocking Time', '2ms', 'Good - nothing to do here', 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07', '16431506305079946651'),
(203, 'Largest Contentful Paint', '1.4s', 'OK, but consider improvement', 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07', '16431506305079946651'),
(204, 'Cumulative Layout Shift', '0.02', 'Good - nothing to do here', 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07', '16431506305079946651'),
(205, 'Performance', '93%', NULL, 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07', '16431506305079946651'),
(206, 'Structure', '73%', NULL, 7, '2022-01-25 20:53:07', '2022-01-25 20:53:07', '16431506305079946651'),
(207, 'First Contentful Paint', '954ms', 'OK, but consider improvement', 8, '2022-01-25 20:55:42', '2022-01-25 20:55:42', '16431506305079946651'),
(208, 'Time to Interactive', '1.1s', 'Good - nothing to do here', 8, '2022-01-25 20:55:42', '2022-01-25 20:55:42', '16431506305079946651'),
(209, 'Speed Index', '1.2s', 'Good - nothing to do here', 8, '2022-01-25 20:55:42', '2022-01-25 20:55:42', '16431506305079946651'),
(210, 'Total Blocking Time', '14ms', 'Good - nothing to do here', 8, '2022-01-25 20:55:42', '2022-01-25 20:55:42', '16431506305079946651'),
(211, 'Largest Contentful Paint', '1.3s', 'Good - nothing to do here', 8, '2022-01-25 20:55:42', '2022-01-25 20:55:42', '16431506305079946651'),
(212, 'Cumulative Layout Shift', '0.02', 'Good - nothing to do here', 8, '2022-01-25 20:55:42', '2022-01-25 20:55:42', '16431506305079946651'),
(213, 'Performance', '95%', NULL, 8, '2022-01-25 20:55:42', '2022-01-25 20:55:42', '16431506305079946651'),
(214, 'Structure', '74%', NULL, 8, '2022-01-25 20:55:42', '2022-01-25 20:55:42', '16431506305079946651'),
(215, 'First Contentful Paint', '959ms', 'OK, but consider improvement', 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53', '16431506305079946651'),
(216, 'Time to Interactive', '1.1s', 'Good - nothing to do here', 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53', '16431506305079946651'),
(217, 'Speed Index', '2.8s', 'Much longer than recommended', 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53', '16431506305079946651'),
(218, 'Total Blocking Time', '21ms', 'Good - nothing to do here', 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53', '16431506305079946651'),
(219, 'Largest Contentful Paint', '1.5s', 'OK, but consider improvement', 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53', '16431506305079946651'),
(220, 'Cumulative Layout Shift', '0.57', 'Much longer than recommended', 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53', '16431506305079946651'),
(221, 'Performance', '74%', NULL, 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53', '16431506305079946651'),
(222, 'Structure', '64%', NULL, 9, '2022-01-25 21:14:53', '2022-01-25 21:14:53', '16431506305079946651'),
(223, 'First Contentful Paint', '1.2s', 'OK, but consider improvement', 10, '2022-01-25 21:17:53', '2022-01-25 21:17:53', '16431506305079946651'),
(224, 'Time to Interactive', '1.5s', 'Good - nothing to do here', 10, '2022-01-25 21:17:53', '2022-01-25 21:17:53', '16431506305079946651'),
(225, 'Speed Index', '3.8s', 'Much longer than recommended', 10, '2022-01-25 21:17:54', '2022-01-25 21:17:54', '16431506305079946651'),
(226, 'Total Blocking Time', '1ms', 'Good - nothing to do here', 10, '2022-01-25 21:17:54', '2022-01-25 21:17:54', '16431506305079946651'),
(227, 'Largest Contentful Paint', '2.7s', 'Much longer than recommended', 10, '2022-01-25 21:17:54', '2022-01-25 21:17:54', '16431506305079946651'),
(228, 'Cumulative Layout Shift', '0.82', 'Much longer than recommended', 10, '2022-01-25 21:17:54', '2022-01-25 21:17:54', '16431506305079946651'),
(229, 'Performance', '60% ', NULL, 10, '2022-01-25 21:17:54', '2022-01-25 21:17:54', '16431506305079946651'),
(230, 'Structure', '52%', NULL, 10, '2022-01-25 21:17:54', '2022-01-25 21:17:54', '16431506305079946651'),
(231, 'First Contentful Paint', '1.0s', 'OK, but consider improvement', 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54', '16431506305079946651'),
(232, 'Time to Interactive', '1.6s', 'Good - nothing to do here', 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54', '16431506305079946651'),
(233, 'Speed Index', '1.9s', 'Longer than recommended', 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54', '16431506305079946651'),
(234, 'Total Blocking Time', '4ms', 'Good - nothing to do here', 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54', '16431506305079946651'),
(235, 'Largest Contentful Paint', '2.5s', 'Much longer than recommended', 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54', '16431506305079946651'),
(236, 'Cumulative Layout Shift', '0.07', 'Good - nothing to do here', 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54', '16431506305079946651'),
(237, 'Performance', '82%', NULL, 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54', '16431506305079946651'),
(238, 'Structure', '52%', NULL, 11, '2022-01-25 21:21:54', '2022-01-25 21:21:54', '16431506305079946651'),
(239, 'First Contentful Paint', '993ms', 'OK, but consider improvement', 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49', '16431506305079946651'),
(240, 'Time to Interactive', '1.1s', 'Good - nothing to do here', 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49', '16431506305079946651'),
(241, 'Speed Index', '1.3s', 'Good - nothing to do here', 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49', '16431506305079946651'),
(242, 'Total Blocking Time', '20ms', 'Good - nothing to do here', 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49', '16431506305079946651'),
(243, 'Largest Contentful Paint', '1.5s', 'OK, but consider improvement', 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49', '16431506305079946651'),
(244, 'Cumulative Layout Shift', '0.09', 'Good - nothing to do here', 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49', '16431506305079946651'),
(245, 'Performance', '92%', NULL, 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49', '16431506305079946651'),
(246, 'Structure', '64%', NULL, 12, '2022-01-25 21:24:49', '2022-01-25 21:24:49', '16431506305079946651'),
(247, 'First Contentful Paint', '974ms', 'OK, but consider improvement', 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05', '16431506305079946651'),
(248, 'Time to Interactive', '1.6s', 'Good - nothing to do here', 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05', '16431506305079946651'),
(249, 'Speed Index', '1.5s', 'OK, but consider improvement', 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05', '16431506305079946651'),
(250, 'Total Blocking Time', '7ms', 'Good - nothing to do here', 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05', '16431506305079946651'),
(251, 'Largest Contentful Paint', '1.8s', 'Longer than recommended', 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05', '16431506305079946651'),
(252, 'Cumulative Layout Shift', '0.09', 'Good - nothing to do here', 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05', '16431506305079946651'),
(253, 'Performance', '88%', NULL, 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05', '16431506305079946651'),
(254, 'Structure', '59%', NULL, 13, '2022-01-25 21:27:05', '2022-01-25 21:27:05', '16431506305079946651'),
(255, 'First Contentful Paint', '926ms', 'Good - nothing to do here', 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48', '16431506305079946651'),
(256, 'Time to Interactive', '1.1s', 'Good - nothing to do here', 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48', '16431506305079946651'),
(257, 'Speed Index', '1.3s', 'OK, but consider improvement', 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48', '16431506305079946651'),
(258, 'Total Blocking Time', '17ms', 'Good - nothing to do here', 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48', '16431506305079946651'),
(259, 'Largest Contentful Paint', '1.1s', 'Good - nothing to do here', 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48', '16431506305079946651'),
(260, 'Cumulative Layout Shift', '0.05', 'Good - nothing to do here', 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48', '16431506305079946651'),
(261, 'Performance', '96%', NULL, 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48', '16431506305079946651'),
(262, 'Structure', '70%', NULL, 14, '2022-01-25 21:33:48', '2022-01-25 21:33:48', '16431506305079946651'),
(263, 'First Contentful Paint', '871ms', 'Good - nothing to do here', 15, '2022-01-25 21:35:56', '2022-01-25 21:35:56', '16431506305079946651'),
(264, 'Time to Interactive', '975ms', 'Good - nothing to do here', 15, '2022-01-25 21:35:56', '2022-01-25 21:35:56', '16431506305079946651'),
(265, 'Speed Index', '978ms', 'Good - nothing to do here', 15, '2022-01-25 21:35:56', '2022-01-25 21:35:56', '16431506305079946651'),
(266, 'Total Blocking Time', '0ms', 'Good - nothing to do here', 15, '2022-01-25 21:35:56', '2022-01-25 21:35:56', '16431506305079946651'),
(267, 'Largest Contentful Paint', '952ms', 'Good - nothing to do here', 15, '2022-01-25 21:35:56', '2022-01-25 21:35:56', '16431506305079946651'),
(268, 'Cumulative Layout Shift', '0.02', 'Good - nothing to do here', 15, '2022-01-25 21:35:56', '2022-01-25 21:35:56', '16431506305079946651'),
(269, 'Performance', '98%', NULL, 15, '2022-01-25 21:35:56', '2022-01-25 21:35:56', '16431506305079946651'),
(270, 'Structure', '75%', NULL, 15, '2022-01-25 21:35:56', '2022-01-25 21:35:56', '16431506305079946651'),
(271, 'First Contentful Paint', '898ms', 'Good - nothing to do here', 16, '2022-01-25 21:37:42', '2022-01-25 21:37:42', '16431506305079946651'),
(272, 'Time to Interactive', '1.0s', 'Good - nothing to do here', 16, '2022-01-25 21:37:42', '2022-01-25 21:37:42', '16431506305079946651'),
(273, 'Speed Index', '1.0s', 'Good - nothing to do here', 16, '2022-01-25 21:37:42', '2022-01-25 21:37:42', '16431506305079946651'),
(274, 'Total Blocking Time', '5ms', 'Good - nothing to do here', 16, '2022-01-25 21:37:42', '2022-01-25 21:37:42', '16431506305079946651'),
(275, 'Largest Contentful Paint', '1.3s', 'OK, but consider improvement', 16, '2022-01-25 21:37:42', '2022-01-25 21:37:42', '16431506305079946651'),
(276, 'Cumulative Layout Shift', '0.02', 'Good - nothing to do here', 16, '2022-01-25 21:37:42', '2022-01-25 21:37:42', '16431506305079946651'),
(277, 'Performance', '96%', NULL, 16, '2022-01-25 21:37:42', '2022-01-25 21:37:42', '16431506305079946651'),
(278, 'Structure', '73%', NULL, 16, '2022-01-25 21:37:42', '2022-01-25 21:37:42', '16431506305079946651'),
(279, 'First Contentful Paint', '889ms', 'Good - nothing to do here', 17, '2022-01-25 21:39:42', '2022-01-25 21:39:42', '16431506305079946651'),
(280, 'Time to Interactive', '971ms', 'Good - nothing to do here', 17, '2022-01-25 21:39:42', '2022-01-25 21:39:42', '16431506305079946651'),
(281, 'Speed Index', '999ms', 'Good - nothing to do here', 17, '2022-01-25 21:39:42', '2022-01-25 21:39:42', '16431506305079946651'),
(282, 'Total Blocking Time', '0ms', 'Good - nothing to do here', 17, '2022-01-25 21:39:42', '2022-01-25 21:39:42', '16431506305079946651'),
(283, 'Largest Contentful Paint', '1.3s', 'OK, but consider improvement', 17, '2022-01-25 21:39:42', '2022-01-25 21:39:42', '16431506305079946651'),
(284, 'Cumulative Layout Shift', '0', 'Good - nothing to do here', 17, '2022-01-25 21:39:42', '2022-01-25 21:39:42', '16431506305079946651'),
(285, 'Performance', '96%', NULL, 17, '2022-01-25 21:39:42', '2022-01-25 21:39:42', '16431506305079946651'),
(286, 'Structure', '75%', NULL, 17, '2022-01-25 21:39:42', '2022-01-25 21:39:42', '16431506305079946651'),
(287, 'First Contentful Paint', '895ms', 'Good - nothing to do here', 18, '2022-01-25 21:43:46', '2022-01-25 21:43:46', '16431506305079946651'),
(288, 'Time to Interactive', '963ms', 'Good - nothing to do here', 18, '2022-01-25 21:43:46', '2022-01-25 21:43:46', '16431506305079946651'),
(289, 'Speed Index', '1.0s', 'Good - nothing to do here', 18, '2022-01-25 21:43:46', '2022-01-25 21:43:46', '16431506305079946651'),
(290, 'Total Blocking Time', '1ms', 'Good - nothing to do here', 18, '2022-01-25 21:43:46', '2022-01-25 21:43:46', '16431506305079946651'),
(291, 'Largest Contentful Paint', '1.2s', 'Good - nothing to do here', 18, '2022-01-25 21:43:46', '2022-01-25 21:43:46', '16431506305079946651'),
(292, 'Cumulative Layout Shift', '0.02', 'Good - nothing to do here', 18, '2022-01-25 21:43:46', '2022-01-25 21:43:46', '16431506305079946651'),
(293, 'Performance', '96%', NULL, 18, '2022-01-25 21:43:46', '2022-01-25 21:43:46', '16431506305079946651'),
(294, 'Structure', '77%', NULL, 18, '2022-01-25 21:43:46', '2022-01-25 21:43:46', '16431506305079946651');

-- --------------------------------------------------------

--
-- Table structure for table `test_details`
--

CREATE TABLE `test_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `testID` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `websiteName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `test_details`
--

INSERT INTO `test_details` (`id`, `testID`, `created_at`, `updated_at`, `websiteName`) VALUES
(2, '16431506305079946651', '2022-01-25 20:43:51', '2022-01-25 20:43:51', 'Delivaz');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `web_pages`
--

CREATE TABLE `web_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pageName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `responseTime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testID` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `web_pages`
--

INSERT INTO `web_pages` (`id`, `pageName`, `responseTime`, `testID`, `created_at`, `updated_at`) VALUES
(7, 'Home', '5', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51'),
(8, 'Country', '5', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51'),
(9, 'City', '5', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51'),
(10, 'Service', '5', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51'),
(11, 'Restaurant', '5', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51'),
(12, 'Food', '5', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51'),
(13, 'Shop', '5', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51'),
(14, 'Product', '5', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51'),
(15, 'AnyThing', '5', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51'),
(16, 'Login', '4', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51'),
(17, 'Register', '4', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51'),
(18, 'Register Shop or Restaurant', '5', 2, '2022-01-25 20:43:51', '2022-01-25 20:43:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_improvements`
--
ALTER TABLE `page_improvements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_improvements_pageid_foreign` (`pageID`);

--
-- Indexes for table `page_jmeter_results`
--
ALTER TABLE `page_jmeter_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_jmeter_results_pageid_foreign` (`pageID`);

--
-- Indexes for table `page_results`
--
ALTER TABLE `page_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_results_pageid_foreign` (`pageID`);

--
-- Indexes for table `test_details`
--
ALTER TABLE `test_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `web_pages`
--
ALTER TABLE `web_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `web_pages_testid_foreign` (`testID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `page_improvements`
--
ALTER TABLE `page_improvements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `page_jmeter_results`
--
ALTER TABLE `page_jmeter_results`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `page_results`
--
ALTER TABLE `page_results`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=295;

--
-- AUTO_INCREMENT for table `test_details`
--
ALTER TABLE `test_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `web_pages`
--
ALTER TABLE `web_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `page_improvements`
--
ALTER TABLE `page_improvements`
  ADD CONSTRAINT `page_improvements_pageid_foreign` FOREIGN KEY (`pageID`) REFERENCES `web_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page_jmeter_results`
--
ALTER TABLE `page_jmeter_results`
  ADD CONSTRAINT `page_jmeter_results_pageid_foreign` FOREIGN KEY (`pageID`) REFERENCES `web_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page_results`
--
ALTER TABLE `page_results`
  ADD CONSTRAINT `page_results_pageid_foreign` FOREIGN KEY (`pageID`) REFERENCES `web_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `web_pages`
--
ALTER TABLE `web_pages`
  ADD CONSTRAINT `web_pages_testid_foreign` FOREIGN KEY (`testID`) REFERENCES `test_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
