/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/***/ (() => {

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

// require('./bootstrap');
Livewire.on('alert', function (param) {
  // alert(param['icon']);
  Swal.fire({
    position: 'center-center',
    icon: param['icon'],
    title: param['title'],
    showConfirmButton: false,
    timer: 4000
  });
});
var nodes = [];
var v;
var adjList;
var countOfNodes;
var s, d;
var counter = 0;
window.addEventListener('test', function (event) {
  var node_path = document.querySelector('.node-path'); // console.log(node_path);
  // console.log("test2");

  nodes = event.detail.nods;
  s = event.detail.start;
  d = event.detail.end; // console.log("nodes", nodes);
  // console.log('details',event.detail);
  // console.log("startNode", s);
  // console.log("endNode", d);
  // console.log("count", Object.keys(nodes).length);

  countOfNodes = Object.keys(nodes).length; // Driver program
  // Create a sample graph
  // nodes.forEach((node)=>{
  //   console.log(node)  ;
  // })

  Graph(countOfNodes); // console.log("countOfNodes",countOfNodes);

  var _loop = function _loop() {
    var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
        key = _Object$entries$_i[0],
        node = _Object$entries$_i[1];

    // console.log(key ,node.name,node.outName);
    // console.log('from' ,node.id);
    node.out.forEach(function (nodeTo) {
      addEdge(node.id, nodeTo); // console.log(node.id,'to' ,nodeTo);
      // console.log('to' ,nodeTo);
    }); // node.outName.forEach((nodeTo)=>{
    //     console.log(node.name,'to' ,nodeTo);
    //
    //     addEdge(node.name, nodeTo);
    //     console.log(node.name,'to' ,nodeTo);
    // console.log('to' ,nodeTo);
    // })
  };

  for (var _i = 0, _Object$entries = Object.entries(nodes); _i < _Object$entries.length; _i++) {
    _loop();
  } // Graph(4);
  // addEdge(0, 1);
  // addEdge(0, 2);
  // addEdge(0, 3);
  // addEdge(2, 0);
  // addEdge(2, 1);
  // addEdge(1, 3);
  // arbitrary source
  // let s = 1;
  // arbitrary destination
  // let d = 5;
  // document.write(
  //     "Following are all different paths from "
  //     + s + " to " + d + "<Br>");
  // node_path.innerHTML +=
  //     "Following are all different paths from "
  //     + s + " to " + d + "<Br>";
  // alert( "Following are all different paths from "
  //     + s + " to " + d);


  printAllPaths(s, d); // node_path.innerHTML ="h hfgh ghfg";
  // A directed graph using
  // adjacency list representation

  function Graph(vertices) {
    // initialise vertex count
    v = vertices; // console.log('v',v);
    // initialise adjacency list

    initAdjList();
  } // utility method to initialise
  // adjacency list


  function initAdjList() {
    adjList = new Array(v); // console.log("v",v);

    for (var i = 0; i <= v; i++) {
      adjList[i] = [];
    }
  } // add edge from u to v


  function addEdge(u, v) {
    // Add v to u's list.
    // console.log("befor");
    adjList[u].push(v); // console.log("after", adjList[u]);
  } // Prints all paths from
  // 's' to 'd'


  function printAllPaths(s, d) {
    // console.log('s',s);
    var isVisited = new Array(v);

    for (var i = 0; i < v; i++) {
      isVisited[i] = false;
    }

    var pathList = []; // add source to path[]

    pathList.push(s); // Call recursive utility
    // console.log('pathList',pathList);
    // console.log('isVisited',isVisited);

    printAllPathsUtil(s, d, isVisited, pathList);
  } // A recursive function to print
  // all paths from 'u' to 'd'.
  // isVisited[] keeps track of
  // vertices in current path.
  // localPathList<> stores actual
  // vertices in the current path


  function printAllPathsUtil(u, d, isVisited, localPathList) {
    // console.log('u',u);
    // console.log('d',d);
    // console.log('isVisited',isVisited);
    if (u == d) {
      // counter +=counter;
      // console.log("counter",counter);
      // console.log('localPathList',localPathList);
      Livewire.emit('newPathDiscovered', JSON.stringify(localPathList)); // console.log('localPathList',localPathList);
      // node_path.innerHTML += localPathList + "<br>";
      // console.log('localPathListl',localPathList);
      // document.write(localPathList + "<br>");
      // if match found then no need to
      // traverse more till depth

      return;
    } // Mark the current node


    isVisited[u] = true; // Recur for all the vertices
    // adjacent to current vertex

    for (var i = 0; i < adjList[u].length; i++) {
      if (!isVisited[adjList[u][i]]) {
        // store current node
        // in path[]
        localPathList.push(adjList[u][i]);
        printAllPathsUtil(adjList[u][i], d, isVisited, localPathList); // remove current node
        // in path[]

        localPathList.splice(localPathList.indexOf(adjList[u][i]), 1);
      }
    } // Mark the current node


    isVisited[u] = false;
  } // This code is contributed by avanitrachhadiya2155
  // alert('Name updated to: ' + event.detail.nods);

}); // JavaScript program to print all
// paths from a source to
// destination.

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/js/app": 0,
/******/ 			"css/app": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunk"] = self["webpackChunk"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["css/app"], () => (__webpack_require__("./resources/js/app.js")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["css/app"], () => (__webpack_require__("./resources/sass/app.scss")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;