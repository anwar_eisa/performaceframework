// require('./bootstrap');

Livewire.on('alert', param => {
    // alert(param['icon']);
    Swal.fire({
        position: 'center-center',
        icon: param['icon'],
        title: param['title'],
        showConfirmButton: false,
        timer: 4000
    })
})
let nodes = [];
let v;
let adjList;
let countOfNodes;
let s,d;
let counter=0;

window.addEventListener('test', event => {


    let node_path = document.querySelector('.node-path');
    // console.log(node_path);
    // console.log("test2");
    nodes = event.detail.nods;
    s = event.detail.start;
    d = event.detail.end;

    // console.log("nodes", nodes);
    // console.log('details',event.detail);
    // console.log("startNode", s);
    // console.log("endNode", d);
    // console.log("count", Object.keys(nodes).length);
    countOfNodes = Object.keys(nodes).length;
    // Driver program
    // Create a sample graph

    // nodes.forEach((node)=>{
    //   console.log(node)  ;
    // })
    Graph(countOfNodes);
    // console.log("countOfNodes",countOfNodes);

    for (const [key, node] of Object.entries(nodes)) {
        // console.log(key ,node.name,node.outName);
        // console.log('from' ,node.id);
        node.out.forEach((nodeTo) => {
            addEdge(node.id, nodeTo);
            // console.log(node.id,'to' ,nodeTo);
            // console.log('to' ,nodeTo);
        });
        // node.outName.forEach((nodeTo)=>{
        //     console.log(node.name,'to' ,nodeTo);
        //
        //     addEdge(node.name, nodeTo);
        //     console.log(node.name,'to' ,nodeTo);
        // console.log('to' ,nodeTo);
        // })
    }

    // Graph(4);
    // addEdge(0, 1);
    // addEdge(0, 2);
    // addEdge(0, 3);
    // addEdge(2, 0);
    // addEdge(2, 1);
    // addEdge(1, 3);

    // arbitrary source
    // let s = 1;

    // arbitrary destination
    // let d = 5;

    // document.write(
    //     "Following are all different paths from "
    //     + s + " to " + d + "<Br>");
    // node_path.innerHTML +=
    //     "Following are all different paths from "
    //     + s + " to " + d + "<Br>";
    // alert( "Following are all different paths from "
    //     + s + " to " + d);
    printAllPaths(s, d);

    // node_path.innerHTML ="h hfgh ghfg";
    // A directed graph using
    // adjacency list representation
    function Graph(vertices) {
        // initialise vertex count
        v = vertices;
        // console.log('v',v);

        // initialise adjacency list
        initAdjList();
    }

    // utility method to initialise
    // adjacency list


    function initAdjList() {
        adjList = new Array(v);
        // console.log("v",v);

        for (let i = 0; i <= v; i++) {
            adjList[i] = [];
        }
    }

    // add edge from u to v
    function addEdge(u, v) {
        // Add v to u's list.
        // console.log("befor");

        adjList[u].push(v);
        // console.log("after", adjList[u]);
    }

    // Prints all paths from
    // 's' to 'd'
    function printAllPaths(s, d) {
        // console.log('s',s);
        let isVisited = new Array(v);
        for (let i = 0; i < v; i++)
            isVisited[i] = false;
        let pathList = [];

        // add source to path[]
        pathList.push(s);
        // Call recursive utility
        // console.log('pathList',pathList);
        // console.log('isVisited',isVisited);

        printAllPathsUtil(s, d, isVisited, pathList);
    }

    // A recursive function to print
    // all paths from 'u' to 'd'.
    // isVisited[] keeps track of
    // vertices in current path.
    // localPathList<> stores actual
    // vertices in the current path
    function printAllPathsUtil(u, d, isVisited, localPathList) {
        // console.log('u',u);
        // console.log('d',d);
        // console.log('isVisited',isVisited);

        if (u == (d)) {
            // counter +=counter;
            // console.log("counter",counter);
            // console.log('localPathList',localPathList);
            Livewire.emit('newPathDiscovered',JSON.stringify(localPathList));
            // console.log('localPathList',localPathList);
            // node_path.innerHTML += localPathList + "<br>";



            // console.log('localPathListl',localPathList);
            // document.write(localPathList + "<br>");
            // if match found then no need to
            // traverse more till depth
            return;
        }

        // Mark the current node
        isVisited[u] = true;

        // Recur for all the vertices
        // adjacent to current vertex
        for (let i = 0; i < adjList[u].length; i++) {
            if (!isVisited[adjList[u][i]]) {
                // store current node
                // in path[]
                localPathList.push(adjList[u][i]);
                printAllPathsUtil(adjList[u][i], d,
                    isVisited, localPathList);

                // remove current node
                // in path[]
                localPathList.splice(localPathList.indexOf
                (adjList[u][i]), 1);
            }
        }

        // Mark the current node
        isVisited[u] = false;
    }


    // This code is contributed by avanitrachhadiya2155

    // alert('Name updated to: ' + event.detail.nods);
})

// JavaScript program to print all
// paths from a source to
// destination.
