<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <style>
        .seventh-step .btn-save-pref, .seventh-step .btn-edit-pref {
            font-size: 17px !important;
            height: 48px !important;
            padding: 0.5rem 1.5rem !important;
        }

        .seventh-step .modal-content {
            border: none;
        }

        .seventh-step .modal-content p {
            color: rgba(0, 0, 0, 0.6);
        }

        .seventh-step .state {
            position: relative;
        }

        .seventh-step .state .improvment {
            padding-bottom: 1rem;
            margin-bottom: 1rem;
        }

        .seventh-step .state .improvment .step-name {
            margin: 0px;
            margin-bottom: 1rem;
            justify-content: start;
        }

        .seventh-step .state .improvment .step-name .content {
            margin: 0px;
        }

        .seventh-step .state .improvment i {
            color: #1ec551;
        }

        .seventh-step .state .improvment header {
            color: rgba(0, 0, 0, 0.85);
            font-weight: bolder;
            font-size: 1.7rem;
        }

        .seventh-step .state .step-name {
            padding: 0rem;
            justify-content: center;
            margin-bottom: -2rem;
        }

        .seventh-step .state .step-name .content {
            background: #fff;
            padding: 0.5rem 1rem;
            margin: 0rem 0.5rem;
            border-radius: 10px;
            border-left: 4px solid #247ec7;
        }

        .seventh-step .state .step-name .content h2 {
            font-family: "Oxygen", sans-serif;
            font-size: 1.5rem;
        }

        .seventh-step .state .step-name .content p {
            font-family: "Oxygen", sans-serif;
            font-size: 1.2rem;
            line-height: 13px;
            margin-bottom: 1rem !important;
            font-weight: bolder;
        }

        .seventh-step .state p {
            font-family: "Oxygen", sans-serif;
            font-size: 1.2rem;
            line-height: 13px;
            margin-bottom: 1rem !important;
            color: rgba(0, 0, 0, 0.6);
        }

        .seventh-step .state .value-description {
            color: rgba(0, 0, 0, 0.85);
            font-size: 0.9rem;
            font-weight: bolder;
        }

        .seventh-step .state .card1 {
            border-radius: 10px;
            border-left: 4px solid #247ec7;
            margin-bottom: 1rem;
            background: #F2F2F2;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.15);
            padding: 0.9rem !important;
        }

        .seventh-step .state .value {
            color: #1ec551;
            font-size: 1.5rem;
            font-weight: bolder;
        }

        .seventh-step .state .table-container {
            padding-top: 2rem;
            background: rgba(255, 255, 255, 0.5);
        }

        .seventh-step .state .table-container .table tr td {
            border: none;
            font-family: "Oxygen", sans-serif;
            color: rgba(0, 0, 0, 0.7);
        }

        .seventh-step .state .table-container .table tbody {
            background: transparent;
        }

        .seventh-step .state .table-container .table tbody .values td {
            color: rgba(0, 0, 0, 0.9);
        }
        .d-flex{
            display: flex;
        }
        .flex-row{
            flex-direction: row;
        }
        .flex-column{
            flex-direction: column;
        }
        .justify-content-start{
            justify-content: start;
        }
        .justify-content-between{
            justify-content: space-between;
        }
        .step-name {
            padding: 1.5rem 0rem;
        }
        .step-name .content {
            background: #fff;
            padding: 0.5rem 1rem;
            margin: 0rem 0.5rem;
            border-radius: 10px;
            border-left: 4px solid #fe2e17;
        }
        .step-name .content h2 {
            font-family: "Arvo", serif;
            font-family: "Bree Serif", serif;
            font-family: "Comfortaa", cursive;
            font-family: "Oxygen", sans-serif;
            font-family: "Patrick Hand", cursive;
            font-family: "Patua One", cursive;
        }
        .step-name .content p {
            font-family: "Arvo", serif;
            font-family: "Comfortaa", cursive;
            font-family: "Oxygen", sans-serif;
            font-size: 23px;
            line-height: 13px;
            margin-bottom: 1rem !important;
        }

        .content {
            background: #fff;
            padding: 0.5rem 1rem;
            margin: 0rem 0.5rem;
            border-radius: 10px;
            border-left: 4px solid #fe2e17;

        }
        .content h2 {
            font-family: 'Arvo', serif;

            font-family: 'Bree Serif', serif;

            font-family: 'Comfortaa', cursive;

            font-family: 'Oxygen', sans-serif;

            font-family: 'Patrick Hand', cursive;

            font-family: 'Patua One', cursive;
        }

        .content p {
            font-family: 'Arvo', serif;

            font-family: 'Comfortaa', cursive;

            font-family: 'Oxygen', sans-serif;

        //font-family: 'Patrick Hand', cursive;
            font-size: 23px;
            line-height: 13px;
            margin-bottom: 1rem !important;
        }
      body{
          background: #EDEBFF;
      }
        .flex-wrap{
            flex-wrap: wrap;
        }
        h2{
            margin: 0px;
        }
        .table{
            width: 100%;
        }
        .table-container{
            margin-bottom: 1rem;
        }
        .container{
            max-width: 1140px;
            margin: auto;

        }
        .px-2{
            padding: 0rem 0.7rem;
        }
        .width-30{
            width: 30%;
        }
        .mt-2 {
            margin-top: .5rem !important;
        }
    </style>
</head>
<body>

<div class="container">

    <div class="seventh-step ">

        <div class="d-flex flex-row step-name">
            <div class="d-flex flex-column content">
                <div class="d-flex flex-row justify-content-start">
                    <h2>{{$websiteNameReporting}} </h2>
                </div>
                <div class="d-flex flex-row justify-content-start">
                    <p> Performance Testing Report</p>
                </div>
            </div>
        </div>

        @if(count($storedResults)!=0)
            @foreach($storedResults as $pageResult)
                @foreach($pageResult['page_j_meter_results'] as $jmeter)

                    @if($jmeter['scenario']=="Load Testing")
                        <div class="state">
                            <div class="d-flex flex-row step-name">
                                <div class="d-flex flex-column content">
                                    <div class="d-flex flex-row justify-content-start">
                                        <h2>{{$pageResult['pageName']}}</h2>
                                    </div>
                                    <div class="d-flex flex-row justify-content-start">
                                        <p> Load Testing</p>
                                    </div>
                                </div>
                            </div>
                            <div class="table-container">
                                <table class="table align-middle table-responsive-xl">
                                    <tbody>
                                    <tr class="">
                                        <td colspan="6">Response Time (ms)</td>

                                        <td> Average</td>
                                        <td> Standard</td>
                                        <td> Deviation</td>
                                        <td>Error Ratio</td>
                                        <td> Throughput</td>

                                    </tr>
                                    <tr>
                                        <td>Min</td>
                                        <td>Median</td>
                                        <td>90th</td>
                                        <td>95th</td>
                                        <td>99th</td>
                                        <td>Max</td>

                                        <td></td>
                                        <td> Deviation</td>
                                        <td>From Expected</td>
                                        <td></td>
                                        <td>(transaction/sec)</td>

                                    </tr>

                                    <tr class="values">
                                        <td>{{$jmeter['min']}}  </td>
                                        <td>{{$jmeter['median']}} </td>
                                        <td>{{$jmeter['90th']}} </td>
                                        <td>{{$jmeter['95th']}}</td>
                                        <td>{{$jmeter['99th']}} </td>
                                        <td>{{$jmeter['max']}} </td>

                                        <td> {{$jmeter['average']}}  </td>
                                        <td> {{$jmeter['deviation']}}   </td>
                                        <td> {{$jmeter['deviation2']}}  </td>
                                        <td>{{$jmeter['error']}} </td>
                                        <td>{{$jmeter['throughput']}} </td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    @endif
                    @if($jmeter['scenario']=="Peak Load Testing")
                        <div class="state">
                            <div class="d-flex flex-row step-name">
                                <div class="d-flex flex-column content">
                                    <div class="d-flex flex-row justify-content-start">
                                        <h2>{{$pageResult['pageName']}}</h2>
                                    </div>
                                    <div class="d-flex flex-row justify-content-start">
                                        <p> Peak Load Testing</p>
                                    </div>
                                </div>
                            </div>
                            <div class="table-container">
                                <table class="table align-middle table-responsive-xl">
                                    <tbody>
                                    <tr class="">
                                        <td colspan="6">Response Time (ms)</td>

                                        <td> Average</td>
                                        <td> Standard</td>
                                        <td> Deviation</td>
                                        <td>Error Ratio</td>
                                        <td> Throughput</td>

                                    </tr>
                                    <tr>
                                        <td>Min</td>
                                        <td>Median</td>
                                        <td>90th</td>
                                        <td>95th</td>
                                        <td>99th</td>
                                        <td>Max</td>

                                        <td></td>
                                        <td> Deviation</td>
                                        <td>From Expected</td>
                                        <td></td>
                                        <td>(transaction/sec)</td>

                                    </tr>

                                    <tr class="values">
                                        <td>{{$jmeter['min']}}  </td>
                                        <td>{{$jmeter['median']}} </td>
                                        <td>{{$jmeter['90th']}} </td>
                                        <td>{{$jmeter['95th']}}</td>
                                        <td>{{$jmeter['99th']}} </td>
                                        <td>{{$jmeter['max']}} </td>

                                        <td> {{$jmeter['average']}}  </td>
                                        <td> {{$jmeter['deviation']}}   </td>
                                        <td> {{$jmeter['deviation2']}}  </td>
                                        <td>{{$jmeter['error']}} </td>
                                        <td>{{$jmeter['throughput']}} </td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    @endif
                    @if($jmeter['scenario']=="Capacity Testing")
                        <div class="state">
                            <div class="d-flex flex-row step-name">
                                <div class="d-flex flex-column content">
                                    <div class="d-flex flex-row justify-content-start">
                                        <h2>{{$pageResult['pageName']}}</h2>
                                    </div>
                                    <div class="d-flex flex-row justify-content-start">
                                        <p> Capacity Testing</p>
                                    </div>
                                </div>
                            </div>
                            <div class="table-container">
                                <table class="table align-middle table-responsive-xl">
                                    <tbody>
                                    <tr class="">
                                        <td colspan="6">Response Time (ms)</td>

                                        <td> Average</td>
                                        <td> Standard</td>
                                        <td> Deviation</td>
                                        <td>Error Ratio</td>
                                        <td> Throughput</td>

                                    </tr>
                                    <tr>
                                        <td>Min</td>
                                        <td>Median</td>
                                        <td>90th</td>
                                        <td>95th</td>
                                        <td>99th</td>
                                        <td>Max</td>

                                        <td></td>
                                        <td> Deviation</td>
                                        <td>From Expected</td>
                                        <td></td>
                                        <td>(transaction/sec)</td>

                                    </tr>
                                    <tr class="values">
                                        <td>{{$jmeter['min']}}  </td>
                                        <td>{{$jmeter['median']}} </td>
                                        <td>{{$jmeter['90th']}} </td>
                                        <td>{{$jmeter['95th']}}</td>
                                        <td>{{$jmeter['99th']}} </td>
                                        <td>{{$jmeter['max']}} </td>

                                        <td> {{$jmeter['average']}}  </td>
                                        <td> {{$jmeter['deviation']}}   </td>
                                        <td> {{$jmeter['deviation2']}}  </td>
                                        <td>{{$jmeter['error']}} </td>
                                        <td>{{$jmeter['throughput']}} </td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    @endif
                    @if($jmeter['scenario']=="Spike Testing")
                        <div class="state">
                            <div class="d-flex flex-row step-name">
                                <div class="d-flex flex-column content">
                                    <div class="d-flex flex-row justify-content-start">
                                        <h2>{{$pageResult['pageName']}}</h2>
                                    </div>
                                    <div class="d-flex flex-row justify-content-start">
                                        <p> Spike Testing</p>
                                    </div>
                                </div>
                            </div>
                            <div class="table-container">
                                <table class="table align-middle table-responsive-xl">
                                    <tbody>
                                    <tr class="">
                                        <td colspan="6">Response Time (ms)</td>

                                        <td> Average</td>
                                        <td> Standard</td>
                                        <td> Deviation</td>
                                        <td>Error Ratio</td>
                                        <td> Throughput</td>

                                    </tr>
                                    <tr>
                                        <td>Min</td>
                                        <td>Median</td>
                                        <td>90th</td>
                                        <td>95th</td>
                                        <td>99th</td>
                                        <td>Max</td>

                                        <td></td>
                                        <td> Deviation</td>
                                        <td>From Expected</td>
                                        <td></td>
                                        <td>(transaction/sec)</td>

                                    </tr>

                                    <tr class="values">
                                        <td>{{$jmeter['min']}}  </td>
                                        <td>{{$jmeter['median']}} </td>
                                        <td>{{$jmeter['90th']}} </td>
                                        <td>{{$jmeter['95th']}}</td>
                                        <td>{{$jmeter['99th']}} </td>
                                        <td>{{$jmeter['max']}} </td>

                                        <td> {{$jmeter['average']}}  </td>
                                        <td> {{$jmeter['deviation']}}   </td>
                                        <td> {{$jmeter['deviation2']}}  </td>
                                        <td>{{$jmeter['error']}} </td>
                                        <td>{{$jmeter['throughput']}} </td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    @endif
                    @if($jmeter['scenario']=="Endurance Testing")
                        <div class="state">
                            <div class="d-flex flex-row step-name">
                                <div class="d-flex flex-column content">
                                    <div class="d-flex flex-row justify-content-start">
                                        <h2>{{$pageResult['pageName']}}</h2>
                                    </div>
                                    <div class="d-flex flex-row justify-content-start">
                                        <p> Endurance Testing</p>
                                    </div>
                                </div>
                            </div>
                            <div class="table-container">
                                <table class="table align-middle table-responsive-xl">
                                    <tbody>
                                    <tr class="">
                                        <td colspan="6">Response Time (ms)</td>

                                        <td> Average</td>
                                        <td> Standard</td>
                                        <td> Deviation</td>
                                        <td>Error Ratio</td>
                                        <td> Throughput</td>

                                    </tr>
                                    <tr>
                                        <td>Min</td>
                                        <td>Median</td>
                                        <td>90th</td>
                                        <td>95th</td>
                                        <td>99th</td>
                                        <td>Max</td>

                                        <td></td>
                                        <td> Deviation</td>
                                        <td>From Expected</td>
                                        <td></td>
                                        <td>(transaction/sec)</td>

                                    </tr>

                                    <tr class="values">
                                        <td>{{$jmeter['min']}}  </td>
                                        <td>{{$jmeter['median']}} </td>
                                        <td>{{$jmeter['90th']}} </td>
                                        <td>{{$jmeter['95th']}}</td>
                                        <td>{{$jmeter['99th']}} </td>
                                        <td>{{$jmeter['max']}} </td>

                                        <td> {{$jmeter['average']}}  </td>
                                        <td> {{$jmeter['deviation']}}   </td>
                                        <td> {{$jmeter['deviation2']}}  </td>
                                        <td>{{$jmeter['error']}} </td>
                                        <td>{{$jmeter['throughput']}} </td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    @endif
                @endforeach


                @if(count($pageResult['page_results'])!=0)

                    <div class="state">
                        <div class="d-flex flex-row step-name">
                            <div class="d-flex flex-column content">
                                <div class="d-flex flex-row justify-content-start">
                                    <h2>{{$pageResult['pageName']}} </h2>
                                </div>
                                <div class="d-flex flex-row justify-content-start">
                                    <p> Main Performance Metrics </p>
                                </div>
                            </div>
                        </div>
                        <div class="table-container px-2">
                            <div class="d-flex flex-row justify-content-between mt-2 flex-wrap">

                                @foreach($pageResult['page_results'] as $p)
                                    <div class="card1 width-30">
                                        <div class="d-flex flex-row justify-content-between">
                                            <p>
                                                {{$p['standard']}}
                                            </p>
                                            <p class="value">{{$p['value']}}</p>
                                        </div>
                                        @if(isset($p['details']))
                                            <br/>
                                            <p class="value-description">  {{$p['details']}}</p>
                                        @endif
                                    </div>
                                @endforeach

                            </div>

                            @if(count($pageResult['page_results'])!=0)

                                <div class="improvment">
                                    <div class="d-flex flex-row step-name">
                                        <div class="d-flex flex-column content width-30">
                                            <div class="d-flex flex-row justify-content-start">
                                                <h2>Page Improvements</h2>
                                            </div>
                                        </div>
                                    </div>

                                    @foreach($pageResult['page_improvement'] as $improve)
                                        <p><i class="fa fa-check mx-2"
                                              aria-hidden="true"></i>{{$improve['improvements']}}</p>
                                    @endforeach
                                </div>
                            @endif

                        </div>
                    </div>
                @endif

            @endforeach

        @endif


    </div>
</div>


</body>
</html>
