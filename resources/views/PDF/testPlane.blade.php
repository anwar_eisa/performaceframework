<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    {{--  --}}
    <style>
        table {
            width: 100%;
            border: 1px solid #000000;
            /*border-collapse: collapse;*/
            border-spacing: -1px;
            margin-bottom: 2rem;
        }

        .row {

            /*border: 1px solid #000000!important;*/
        }

        tr td:first-child {
            font-weight: bolder;
            width: 150px;
        }

        .Workload td:first-child {
            width: auto!important;
            font-weight: normal;
        }

        .Workload tr:nth-child(2) td {
            font-weight: bolder !important;

        }

        td {
            padding: 0.5rem;
            border: 1px solid #000000 !important;
            line-height: 1.5rem;
            font-size: 16px;
        }

        .address {
            display: flex;
            flex-direction: row;
            justify-content: center;
            font-weight: bolder;
            font-size: 18px;
            padding: 1rem 0rem;
        }

        .step-cell {
            /*width: 300px;*/
            min-width: 150px;
            padding: 0px;
        }

        .step {
            display: flex;
            width: 94%;
            padding: 0.5rem;
            border-top: solid 1px #000000;
            border-width: thin;
        }

        .page {
            width: 50%;
        }

        .think-time {
            width: 50%;
        }
    </style>
</head>
<body>
<table>
    <tbody>
    <tr class="row">
        <td>
            Document
        </td>
        <td>
            Performance Testing Plane For {{$testName}}
        </td>
    </tr>
    <tr class="row">
        <td>
            Test ID
        </td>
        <td>
            {{$testID}}
        </td>
    </tr>
    <tr class="row">
        <td>
            Website
        </td>
        <td>
            {{$website}}
        </td>
    </tr>
    <tr class="row">
        <td>Objective</td>
        <td>
            Develop Performance test Plan for the {{$website}} website
            to ensure if the current system architecture is optimal for handling increased
            traffic on the website and also, to figure out if there are any upgrades that need to be done.

        </td>
    </tr>
    <tr class="row">
        <td>
            Scope
        </td>
        <td>
            1-Performing the user experience performance simulator without a specific load. <br/>
            2-Performing the user experience performance simulator with a Expected Normal load (Load Testing). <br/>
            3-Performing the user experience performance simulator with a Expected Peak load (Peak Load Testing). <br/>
            4-Performing the user experience performance simulator with a Expected Future load (Capacity Testing).
            <br/>
            5-Performing the user experience performance simulator with a Un Expected load (Spike Testing). <br/>
            6-Performing the user experience performance simulator with a Expected load During a Long Time (Endurance
            Testing). <br/>
            7-Propose suggestions
        </td>

    </tr>

    <tr class="row">
        <td>
            Out of Scope
        </td>
        <td>
            1-Functional or accuracy testing of the {{$website}} website.<br/>
            2-Any other testing types not included in the Scope section.
        </td>

    </tr>
    <tr class="row">
        <td>
            Hardware
        </td>
        <td>
            Load Generator, Ex: (Dell Intel Core i7 -3.40 GHz., 16GB RAM).<br/>

        </td>

    </tr>

    <tr class="row">
        <td>
            Software
        </td>
        <td>
            Operating System, EX: (Windows Or Linux Or Mac)
        </td>

    </tr>

    <tr class="row">
        <td>
            Performance Testing Tool
        </td>
        <td>
            1-GTmetrix.<br/>
            2-Apache-JMeter-5.4.1.
        </td>

    </tr>

    <tr class="row">
        <td>
            Environment
        </td>
        <td>
            1-Java Runtime Environment (JRE) is installed to run JMeter.<br/>
            2-A good internet connection is Confirmed.

        </td>

    </tr>

    <tr class="row">
        <td>
            Risks
        </td>
        <td>
            The load JMeter applying is not the only load for the web site.
            Also, at the same time, there will be a website getting requests from the outside world.
            So, the report analysis might be not very correct.
            The Best Time To Execute This Plane After 11 PM.
        </td>

    </tr>

    <tr class="row">
        <td>
            Performance Test Scenarios
        </td>
        <td>
            Scenarios are explained in: <br/>
            1-Scenario 1 For One User.<br/>
            2-Scenario 2 For Load Testing.<br/>
            3-Scenario 3 For Peak Load Testing.<br/>
            4-Scenario 4 For Capacity Testing.<br/>
            5-Scenario 5 For Spike Testing.<br/>
            6-Scenario 6 For Endurance Testing.<br/>
        </td>
    </tr>

    <tr class="row">
        <td>
            Entry Criteria
        </td>
        <td>
            Functionally stable application.
        </td>
    </tr>
    <tr class="row">
        <td>
            Exit Criteria:
        </td>
        <td>
            All the Performance objectives are met.
        </td>
    </tr>
    </tbody>
</table>


<table>
    <tbody>
    <tr class="row">
        <td>Scenario 1</td>
        <td>Test user Experience with out load</td>

    </tr>

    <tr class="row">
        <td>Objective</td>
        <td>
            Validate Response Time For All Pages in {{$website}} By measuring:<br/>
            1-First Contentful Paint.<br/>
            2-Time to Interactive.<br/>
            3-Speed Index. <br/>
            4-Total Blocking Time. <br/>
            5-Largest Contentful Paint. <br/>
            6-Cumulative Layout Shift.
        </td>

    </tr>

    <tr class="row">
        <td> Performance Testing Tool</td>
        <td>GTmetrix</td>
    </tr>

    <tr class="row">
        <td>Approach and Execution Strategy</td>
        <td>
            1-Select Any Browser.<br>
            2-Go To https://gtmetrix.com/ <br/>
            3-Enter URL For Target Page.
        </td>

    </tr>
    <tr class="row">
        <td> Results</td>
        <td>
            Report For Every Page Contain ResponseTime Details.
        </td>

    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr class="row">
        <td> Scenario 2</td>
        <td> Test user Experience with Expected Normal Load</td>

    </tr>

    <tr class="row">
        <td>Objective</td>
        <td> Validate Response Time For Every Page in {{$website}} </td>

    </tr>

    <tr class="row">
        <td>Performance Testing Tool</td>
        <td>Apache-JMeter-5.4.1</td>
    </tr>

    <tr class="row">
        <td>Duration</td>
        <td>
            Ramp-Up period: {{$loadInfo['normal']['duration']*60}} seconds
        </td>
    </tr>

    <tr class="row">
        <td>Approach and Execution Strategy</td>
        <td>
            Apply Workload modelling For Load testing On JMeter
        </td>

    </tr>
    <tr class="row">
        <td> Results</td>
        <td>
            Report Contain Throughput For Every Page.
        </td>

    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr class="row">
        <td> Scenario 3</td>
        <td> Test user Experience with Expected Peak Load</td>

    </tr>

    <tr class="row">
        <td>Objective</td>
        <td> Validate Response Time For Every Page in {{$website}} </td>

    </tr>

    <tr class="row">
        <td>Performance Testing Tool</td>
        <td>Apache-JMeter-5.4.1</td>
    </tr>

    <tr class="row">
        <td>Duration</td>
        <td>
            Ramp-Up period: {{$loadInfo['peak']['duration']*60 }} seconds
        </td>
    </tr>

    <tr class="row">
        <td>Approach and Execution Strategy</td>
        <td>
            Apply Workload modelling For Peak testing On JMeter
        </td>

    </tr>
    <tr class="row">
        <td> Results</td>
        <td>
            Report Contain Throughput For Every Page.
        </td>

    </tr>
    </tbody>

</table>

<table>
    <tbody>
    <tr class="row">
        <td> Scenario 4</td>
        <td> Test user Experience with Expected Future Load</td>

    </tr>

    <tr class="row">
        <td>Objective</td>
        <td> Validate Response Time For Every Page in {{$website}} </td>
    </tr>

    <tr class="row">
        <td>Performance Testing Tool</td>
        <td>Apache-JMeter-5.4.1</td>
    </tr>

    <tr class="row">
        <td>Duration</td>
        <td>
            Ramp-Up period: {{$loadInfo['future']['duration']*60}} seconds
        </td>
    </tr>

    <tr class="row">
        <td>Approach and Execution Strategy</td>
        <td>
            Apply Workload modelling For Capacity testing On JMeter
        </td>

    </tr>
    <tr class="row">
        <td> Results</td>
        <td>
            Report Contain Throughput For Every Page.
        </td>

    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr class="row">
        <td> Scenario 5</td>
        <td> Test user Experience in Spike Testing</td>

    </tr>

    <tr class="row">
        <td>Objective</td>
        <td> Validate Response Time For Every Page in {{$website}} And  Server Resource Monitoring</td>
    </tr>

    <tr class="row">
        <td>Performance Testing Tool</td>
        <td>Apache-JMeter-5.4.1</td>
    </tr>

    <tr class="row">
        <td>Duration</td>
        <td>
            Ramp-Up period: {{$loadInfo['normal']['user']}} seconds
        </td>
    </tr>

    <tr class="row">
        <td>Approach and Execution Strategy</td>
        <td>
            Apply Workload modelling For Load testing On JMeter
        </td>

    </tr>
    <tr class="row">
        <td> Results</td>
        <td>
            Report Contain Throughput For Every Page.
        </td>

    </tr>
    </tbody>

</table>

<table>
    <tbody>
    <tr class="row">
        <td> Scenario 6</td>
        <td> Test user Experience with Expected Normal Load With Long Duration</td>

    </tr>

    <tr class="row">
        <td>Objective</td>
        <td> Validate Response Time For Every Page in {{$website}} And Server Resource Monitoring </td>
    </tr>

    <tr class="row">
        <td>Performance Testing Tool</td>
        <td>Apache-JMeter-5.4.1</td>
    </tr>

    <tr class="row">
        <td>Duration</td>
        <td>
            Ramp-Up period: {{$loadInfo['endurance']['duration']*60}} seconds
        </td>
    </tr>

    <tr class="row">
        <td>Approach and Execution Strategy</td>
        <td>
            Apply Workload modelling For Capacity testing On JMeter
        </td>

    </tr>
    <tr class="row">
        <td> Results</td>
        <td>
            Report Contain Throughput For Every Page.
        </td>

    </tr>
    </tbody>
</table>
<table class="Workload">
    <tbody>
    <tr class="row">
        <td colspan="9" class="align-middle">
            Workload modelling<br/>
            NL: Number of Users in Load testing<br/>
            NP: Number of Users in Peak Load testing<br/>
            NC: Number of Users in Capacity testing<br/>
            ND: Number of Users in Endurance testing<br/>
            Z: Page Think Time (seconds)<br/>
            P: Pacing (seconds)<br/>
            L: Loop Count<br/>
            TT: Time To End Iteration By One User (seconds)

        </td>
    </tr>
    <tr class="row">
        <td style="border-right: none!important">Step</td>
        <td style="border: none!important;border-bottom: 1px solid #000000!important;border-top: 1px solid #000000!important;"></td>
        <td>NL</td>
        <td>NP</td>
        <td>NC</td>
        <td>ND</td>
        <td>P</td>
        <td>L</td>
        <td>TT</td>

    </tr>
    <?php $i = 1; ?>
    @foreach($iterations as $iteration)
        <?php $timeForIterations = 0; ?>
        <tr  class="row">
            <td colspan="2" style="font-weight: normal!important;">
                <?php $g = 1; ?>
                @foreach($iteration['pages'] as $page)
                    {{$g}}- &nbsp;{{$pages[$page]['name']}} &nbsp;&nbsp;&nbsp;

                    Z= {{$pages[$page]['thinkTime']}} <br/>


                    <?php $g++;
                    $pageTime = $pages[$page]['thinkTime'] + $pages[$page]['responseTime'];
                    $timeForIterations += $pageTime;
                    ?>
                @endforeach
            </td>
            <td>{{round($iteration['normalUser'])}}</td>
            <td>{{round($iteration['peakUser'])}}</td>
            <td>{{round($iteration['futureUser'])}}</td>
            <td>{{round($iteration['enduranceUser'])}}</td>
            <td>{{$iteration['pacing']}}</td>
            <td>{{$iteration['loopCount']}}</td>
            <td>{{$timeForIterations+$iteration['pacing']}}</td>

        </tr>
        <?php $i++; ?>
    @endforeach
    </tbody>
</table>
</body>
</html>
