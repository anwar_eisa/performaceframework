<div class="home-page">
    <div class="container">
        @if($firstStep)

            <div class="d-flex flex-row step-name">
                {{--                <svg width="9" height="70" viewBox="0 0 9 55" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
                {{--                    <rect x="3" y="4" width="3" height="100" rx="1.5" fill="#FFCBDE"/>--}}
                {{--                    <rect width="9" height="35" rx="4.5" fill="#FE2E17"/>--}}
                {{--                </svg>--}}

                <div class="d-flex flex-column content">
                    <div class="d-flex flex-row justify-content-start">
                        <h2>First step </h2>
                    </div>
                    <div class="d-flex flex-row justify-content-start">
                        <p> Get Information About Your Website</p>
                    </div>
                </div>
            </div>
            <div class="fifth-step ">
                <div class="d-flex flex-row justify-content-between width-full">
                    <div class="width-40 input-container">
                        <label class="form-label" for="form12">Enter Website Name</label>
                        <input type="text" id="form12" class="form-control" wire:model.defer="websiteName"/>
                    </div>

                </div>

                <div class="d-flex flex-row justify-content-between width-full">
                    <div class="width-40 input-container">
                        <label class="form-label" for="form12">Normal Load (users) Expected</label>
                        <input type="text" id="form12" class="form-control" wire:model.defer="normalUserLoad"/>
                    </div>
                    <div class="width-40 input-container">
                        <label class="form-label" for="form12">Duration (Minutes)</label>
                        <input type="text" id="form12" class="form-control" wire:model.defer="normalLoadDuration"/>
                    </div>
                </div>

                <div class="d-flex flex-row justify-content-between width-full">
                    <div class="width-40 input-container">
                        <label class="form-label" for="form12">Peak Load (users) Expected</label>
                        <input type="text" id="form12" class="form-control" wire:model.defer="peakUserLoad"/>
                    </div>
                    <div class="width-40 input-container">
                        <label class="form-label" for="form12">Duration (Minutes)</label>
                        <input type="text" id="form12" class="form-control" wire:model.defer="peakLoadDuration"/>
                    </div>
                </div>

                <div class="d-flex flex-row justify-content-between width-full">
                    <div class="width-40 input-container">
                        <label class="form-label" for="form12">Future Load (users) Expected</label>
                        <input type="text" id="form12" class="form-control" wire:model.defer="futureUserLoad"/>
                    </div>
                    <div class="width-40 input-container">
                        <label class="form-label" for="form12">Duration (Minutes) </label>
                        <input type="text" id="form12" class="form-control" wire:model.defer="futureLoadDuration"/>
                    </div>
                </div>

                <div class="d-flex flex-row justify-content-between width-full">

                    <div class="width-40 input-container">
                        <label class="form-label" for="form12">The longest expected time can be maintained on the load
                            continuously (Hours) </label>

                        <div class="form-check form-check-inline">
                            <input wire:model.defer="enduranceDuration" class="form-check-input" type="radio"
                                   name="inlineRadioOptions" id="inlineRadio5" value="3">
                            <label class="form-check-label" for="inlineRadio5">3 Hours</label>
                        </div>

                        <div class="form-check form-check-inline">
                            <input wire:model.defer="enduranceDuration" class="form-check-input" type="radio"
                                   name="inlineRadioOptions" id="inlineRadio6" value="6">
                            <label class="form-check-label" for="inlineRadio6">6 Hours</label>
                        </div>

                        <div class="form-check form-check-inline">
                            <input wire:model.defer="enduranceDuration" class="form-check-input" type="radio"
                                   name="inlineRadioOptions" id="inlineRadio1" value="12">
                            <label class="form-check-label" for="inlineRadio1">12 Hours</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input wire:model.defer="enduranceDuration" class="form-check-input" type="radio"
                                   name="inlineRadioOptions" id="inlineRadio2" value="24">
                            <label class="form-check-label" for="inlineRadio2">24 Hours</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input wire:model.defer="enduranceDuration" class="form-check-input" type="radio"
                                   name="inlineRadioOptions" id="inlineRadio1" value="36">
                            <label class="form-check-label" for="inlineRadio1">36 Hours</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input wire:model.defer="enduranceDuration" class="form-check-input" type="radio"
                                   name="inlineRadioOptions" id="inlineRadio2" value="72">
                            <label class="form-check-label" for="inlineRadio2">72 Hours</label>
                        </div>
                    </div>
                </div>

                <div class="d-flex flex-row justify-content-center mt-3">
                    <button class="btn  btn-edit-pref" wire:click.prevent="saveUserLoadConfig"> save</button>
                </div>

            </div>

            <div class="d-flex flex-row width-full">
                <div class="d-flex flex-column width-50 p-2">
                    @foreach($pages as $page)
                        <div class="input-container @if($page!=$i) d-none @endif">
                            <label class="form-label" for="form12">Page Name</label>
                            <input type="text" id="form12" class="form-control" wire:model.defer="pagesName.{{$page}}"/>
                        </div>

                        <div class="input-container @if($page!=$i) d-none @endif">
                            <label class="form-label" for="form12"> Expected Response Time (R) For Page in
                                Seconds</label>
                            <input type="number" step="0.001" id="form12" class="form-control"
                                   wire:model.defer="pagesResponseTime.{{$page}}"/>
                        </div>
                        <div class="input-container @if($page!=$i) d-none @endif">
                            <label class="form-label" for="form12">Think Time (Z) For Page In Second</label>
                            <input type="number" step="0.001" id="form12" class="form-control"
                                   wire:model.defer="pagesThinkTime.{{$page}}"/>
                        </div>

                        <div class="d-flex flex-row justify-content-start  mt-2 @if($page!=$i) d-none @endif">
                            <button class="btn btn-save-pref"
                                    wire:click.prevent="addNextPage({{$page}})">Add Page
                            </button>
                            {{--                  <button class="btn btn-primary  btn-primary-customize" wire:click.prevent="submit">Next Step</button>--}}
                        </div>
                    @endforeach


                </div>
                @if($allPagesInFirst)
                    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                        <symbol id="check-circle-fill" fill="#2CC990" viewBox="0 0 16 16">
                            <path
                                d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                        </symbol>
                        <symbol id="info-fill" fill="#fff" viewBox="0 0 16 16">
                            <path
                                d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
                        </symbol>
                        <symbol id="exclamation-triangle-fill" fill="#fff" viewBox="0 0 16 16">
                            <path
                                d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                        </symbol>
                    </svg>
                    <div class="d-flex flex-column width-50 p-2">
                        <div class="pages-container d-flex align-items-center d-flex flex-column width-full "
                             role="alert">
                            <div class="width-full d-flex flex-row justify-content-between  header">
                                <div class="w-50">
                                    <p>Your Pages</p>
                                </div>
                                <div class="w-50 d-flex flex-row justify-content-end">
                                    <p class=""> Delete All Pages</p> &nbsp;&nbsp;
                                    <button type="button" class="btn  btn-close"
                                            wire:click.prevent="deleteAllPages"></button>
                                </div>

                            </div>
                            @foreach($allPagesInFirst as $key=>$page)
                                <div class="d-flex flex-row justify-content-between width-full  pages">
                                    <div class="w-75 d-flex flex-row justify-content-start">
                                        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img"
                                             aria-label="Success:">
                                            <use xlink:href="#check-circle-fill"/>
                                        </svg>
                                        {{--                           <div>--}}
                                        <p>{{$page['name']}}, &nbsp;R=({{$page['responseTime']}})s&nbsp;
                                            Z=({{$page['thinkTime']}})s </p>
                                        {{--                           </div>--}}
                                    </div>
                                    <div class="w-25 d-flex flex-row justify-content-end">
                                        <button type="button" class=" btn-close"
                                                wire:click.prevent="removePage({{$key}})"></button>
                                    </div>

                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>


            <div
                class="d-flex flex-row justify-content-center width-full  mt-2 @if(!isset($allPagesInFirst)) d-none @endif">
                <button class="btn btn-edit-pref" wire:click.prevent="startSecondPage">
                    Next Step
                </button>
            </div>
        @endif
        @if($zeroStep)

            <div class="zero-step">
                <div class="d-flex flex-column justify-content-center content">
                    <div class="heading">
                        {{--                        PrefTester Is The best way to generate professional web performance--}}
                        {{--                        testing plan and analyze plan implementation results--}}
                        PrefTester Is The best wayTo predict user
                        performance in different Load Levels
                    </div>
                    <div class="d-flex flex-row justify-content-start">
                        <button class="btn  btn-edit-pref"
                                wire:click.prevent="startMyTest">Start Your Test Now
                        </button>
                    </div>
                    <div class="d-flex flex-row justify-content-start">
                        <button class="btn  btn-edit-pref btn-report"
                                wire:click.prevent="goToSixStep">Generate Report
                        </button>
                    </div>
                </div>

                <div class="content">
                    <div class="img">
                        <img src="{{asset('img/banner.png')}}" alt="">
                    </div>
                </div>


            </div>

        @endif
        @if($secondStep)
            <div class="second-step">
                <div class="step-name d-flex flex-row">
                    <div class="d-flex flex-column content">
                        <div class="d-flex flex-row justify-content-start ">
                            <h2>Second Step</h2>
                        </div>
                        <div class="d-flex flex-row justify-content-start">
                            <p> Determine Navigation In Your Website</p>
                        </div>
                    </div>

                </div>

                @foreach($allPagesInSecond as $mainKey=>$mainArrayPage)
                    <div class="@if($mainKey!=$firstPageInSecond) d-none @endif">


                        <div class="d-flex flex-row justify-content-center page-header">
                            {{--                    <div class="d-flex flex-column align-items-center align-content-center justify-content-center">--}}
                            {{--                        <i class="fas fa-arrow-right fa-1x"></i>--}}
                            {{--                        <i class="fas fa-arrow-left fa-1x"></i>--}}
                            {{--                    </div>--}}
                            &nbsp;&nbsp;
                            <h3 class="page-address"> {{$mainArrayPage['name']}}</h3>&nbsp;&nbsp;
                            {{--                    <div class="d-flex flex-column align-items-center align-content-center justify-content-center">--}}
                            {{--                        <i class="fas fa-arrow-right fa-1x"></i>--}}
                            {{--                        <i class="fas fa-arrow-left fa-1x"></i>--}}
                            {{--                    </div>--}}
                        </div>
                        <div class="d-flex flex-row width-full">

                            <div class="row w-50">
                                <div
                                    class="d-flex flex-row justify-content-start mt-3  align-content-center align-items-center page-navigation">
                                    {{--                                                <i class="fas fa-arrow-right fa-1x"></i> &nbsp;&nbsp;--}}
                                    <h4>Pages Refer To This Page (In)</h4>
                                </div>
                                <div>
                                    @foreach($allPagesInSecond as $key=>$arrayPage)
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" value="{{$arrayPage['id']}}" type="checkbox"
                                                   id="inlineCheckboxIn.{{$arrayPage['id']}}.{{$arrayPage['name']}}"
                                                   wire:model="pageIN.{{$arrayPage['id']}}">
                                            <label class="form-check-label"
                                                   for="inlineCheckboxIn.{{$arrayPage['id']}}.{{$arrayPage['name']}}">
                                                {{$arrayPage['name']}}
                                            </label>
                                        </div>
                                    @endforeach

                                    <div class="d-flex flex-row justify-content-start mt-2">
                                        <button class="btn  @if(!$savedIn) btn-save-pref @else btn-saved-pref @endif"
                                                wire:click.prevent="submitIn({{$mainKey}})">
                                            @if($savedIn)
                                                Saved
                                            @else
                                                Save
                                            @endif
                                        </button>
                                    </div>

                                </div>

                            </div>

                            <div class="row w-50">
                                <div
                                    class="d-flex flex-row justify-content-start mt-3  align-content-center align-items-center page-navigation">

                                    {{--                                                <i class="fas fa-arrow-left fa-1x"></i> &nbsp;&nbsp;--}}
                                    <h4> This Page Refer To (Out)</h4>
                                </div>
                                <div>
                                    @foreach($allPagesInSecond as $key=>$arrayPage)
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" value="{{$arrayPage['id']}}" type="checkbox"
                                                   id="inlineCheckboxOut.{{$arrayPage['id']}}.{{$arrayPage['name']}}"
                                                   wire:model="pageOut.{{$arrayPage['id']}}">
                                            <label class="form-check-label"
                                                   for="inlineCheckboxOut.{{$arrayPage['id']}}.{{$arrayPage['name']}}">
                                                {{$arrayPage['name']}}
                                            </label>
                                        </div>
                                    @endforeach

                                    <div class="d-flex flex-row justify-content-start mt-2">
                                        <button class="btn  @if(!$savedOut) btn-save-pref @else btn-saved-pref @endif"
                                                wire:click.prevent="submitOut({{$mainKey}})">
                                            @if($savedOut)
                                                Saved
                                            @else
                                                Save
                                            @endif
                                        </button>
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div class="d-flex flex-row justify-content-between width-full mt-3 page-navegation">
                            <div class="d-flex flex-row justify-content-start">
                                <button class="btn  btn-cancle2-pref" wire:click.prevent="previousPageNavigate"
                                        @if($allPagesInSecond[$this->firstKeyInSecond]['id']==$firstPageInSecond) disabled @endif>
                                    Previous
                                    Page
                                </button>
                            </div>
                            <div class="d-flex flex-row justify-content-start">
                                <button class="btn  btn-edit-pref" wire:click.prevent="nextPageNavigate"
                                        @if($lastKeyInSecond==$firstPageInSecond) disabled @endif>Next Page
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach


                <div class="d-flex flex-row justify-content-center mt-3">
                    <button class="btn btn-edit-pref" wire:click.prevent="goToThirdStep">Next Step</button>
                </div>

                <div class="d-flex flex-row justify-content-center mt-3">
                    <button class="btn btn-cancle2-pref" wire:click.prevent="startMyTest">Previous Step</button>
                </div>
            </div>

        @endif

        @if($thirdStep)
            <div class="third-step  @if(!$thirdStep) d-none @endif">
                <div class="d-flex flex-row step-name">
                    <div class="d-flex flex-column content">
                        <div class="d-flex flex-row justify-content-start">
                            <h2>Third Step</h2>
                        </div>
                        <div class="d-flex flex-row justify-content-start">
                            <p> Generate Available Iterations</p>
                        </div>
                    </div>
                </div>

                {{--            <div class="d-flex flex-row justify-content-center page-header">--}}
                {{--                <h3 class="page-address"> {{$mainArrayPage['name']}}</h3>&nbsp;&nbsp;--}}
                {{--            </div>--}}
                <div class="d-flex flex-row width-full">
                    <div class="d-flex flex-column width-30">
                        <h4> Start Node (Page) </h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                    data-bs-toggle="dropdown" aria-expanded="false">

                                @if($firstNode)
                                    {{data_get($this->firstNodeName, $firstNode.'.name')}}
                                    {{--                            {{$firstNode}}--}}
                                @else
                                    Select Start Page
                                @endif
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                @foreach($this->sortDataSourceByRank($this->dataSource) as $arrayPage)
                                    <li><a class="dropdown-item" href="#"
                                           wire:click.prevent="$set('firstNode', {{$arrayPage['id']}})">{{$arrayPage['name']}}
                                            : Rank is {{round($arrayPage['rank'], 3)}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="d-flex flex-column width-30">
                        <h4> End Node (Page) </h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                @if($endNode)
                                    {{data_get($this->lastNodeName, $endNode.'.name')}}
                                    {{--                                {{$endNode}}--}}
                                @else
                                    Select End Page
                                @endif
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                {{--                            {{dd($dataSource)}}--}}

                                @foreach($this->sortDataSourceByRank($this->dataSource) as $arrayPage)
                                    <li><a class="dropdown-item" href="#"
                                           wire:click.prevent="$set('endNode', {{$arrayPage['id']}})">{{$arrayPage['name']}}
                                            : Rank is {{round($arrayPage['rank'], 3)}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="d-flex flex-row width-40">

                        <div class="d-flex flex-column">
                            <h4>Through This Nodes </h4>
                            <select class="form-select" size="3" aria-label="size 3 select example"
                                    wire:model.lazy="throughPages" multiple>
                                {{--                                <option selected>Open this select menu</option>--}}
                                @foreach($dataSource as $page)
                                    <option value="{{$page['id']}}">{{$page['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="d-flex flex-row justify-content-center width-full mt-3">
                    <button class="btn btn-edit-pref" wire:click.prevent="generateIterations">Generate Iterations
                    </button>
                </div>

                <div class="d-flex flex-column justify-content-start">
                    @if(count($iterationsGenerated)!=0)
                        <form wire:submit.prevent="saveIterations">
                            {{--                        {{dd($iterationsGenerated)}}--}}
                            @foreach($iterationsGenerated as $key=>$pathInArray)
                                <div
                                    class="form-check d-flex flex-row justify-content-start  align-items-center align-content-center">
                                    <input class="form-check-input" type="checkbox" id="iterations{{$key}}"
                                           wire:change.prevent="saveIterations({{$key}})"
                                           @if(isset($selectedIterations[$key]) && $selectedIterations[$key]==true) checked @endif >
                                    <label class="form-check-label" for="iterations{{$key}}">
                                        <div class="d-flex flex-row justify-content-start">
                                            <p class="iterations">
                                                @foreach($pathInArray as $key=>$path)
                                                    {{--                                                {{dd($path,$dataSource)}}--}}
                                                    @if(array_key_first($pathInArray)!=$key)
                                                        <i class="fas fa-arrow-right "></i>
                                                    @endif
                                                    {{data_get($dataSource, $path.'.name')}}
                                                @endforeach
                                            </p>
                                        </div>
                                    </label>
                                </div>

                            @endforeach
                            {{--                        <div class="d-flex flex-row justify-content-center mt-3">--}}
                            {{--                            <button type="submit" class="btn btn-primary">Save Selected Iterations</button>--}}
                            {{--                        </div>--}}
                        </form>



                    @endif
                    @if(count($iterationsGenerated)==0 and $endNode!=null and $firstNode!=null and $pathDiscoverResult==true)
                        <div class="alert alert-info">
                            There Is No Path Try Another Nodes
                        </div>
                    @endif
                </div>


            </div>
            <div class="d-flex flex-row justify-content-center mt-3">
                <button class="btn  btn-edit-pref" wire:click.prevent="goToFourthStep"> Next Step</button>
            </div>
            <div class="d-flex flex-row justify-content-center mt-3">
                <button class="btn btn-cancle2-pref" wire:click.prevent="startSecondPage">Previous Step</button>
            </div>


        @endif

        @if($fourthStep)
            <div class="fourth-step  @if(!$fourthStep) d-none @endif">
                <div class="d-flex flex-row step-name">
                    <div class="d-flex flex-column content">
                        <div class="d-flex flex-row justify-content-start">
                            <h2>Fourth Step</h2>
                        </div>
                        <div class="d-flex flex-row justify-content-start">
                            <p>Set Iterations Setting</p>
                        </div>
                    </div>
                </div>

            </div>

            <div class="table-responsive">
                <table class="table align-middle">
                    <thead>

                    <tr>
                        <th>
                            Number
                        </th>
                        <th>
                            Steps
                        </th>

                        <th>
                            Pacing Time (Seconds)
                        </th>
                        <th>
                            loop Count For User
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $j = 1;
                    ?>
                    @foreach($selectedIterations as $iterationKey=>$iteration)
                        <tr>
                            <td class="align-middle">{{$j}}</td>
                            <td class="align-middle">
                                <?php $g = 1; ?>
                                @foreach($iteration['pages'] as $page)
                                    <div class="mt-2"> {{$g}}- &nbsp;{{$dataSource[$page]['name']}}</div>
                                    <?php $g++; ?>
                                @endforeach
                            </td>
                            {{--                        <form wire:submit.prevent="SaveIterationConfig({{$iterationKey}})">--}}
                            <td class="align-middle">
                                <div class="">
                                    <input type="number" step="0.001" id="form12" class="form-control"
                                           wire:model.defer="pacing.{{$iterationKey}}"/>
                                </div>
                            </td>
                            <td class="align-middle">
                                <div class="">
                                    <input type="number" id="form12" class="form-control"
                                           wire:model.defer="iterationLoop.{{$iterationKey}}"/>
                                </div>
                            </td>
                            <td class="align-middle">
                                <button
                                    wire:click.prevent="SaveIterationConfig({{$iterationKey}},{{$j}})"
                                    type="button"
                                    class="btn btn-save-pref">
                                    Save
                                </button>
                            </td>
                            {{--                        </form>--}}
                        </tr>
                        <?php  $j++;?>
                    @endforeach
                    </tbody>
                </table>
            </div>



            <div class="d-flex flex-row justify-content-center mt-3">
                <button class="btn  btn-edit-pref" wire:click.prevent="goToFifthStep"> Next Step</button>
            </div>
            <div class="d-flex flex-row justify-content-center mt-3">
                <button class="btn  btn-cancle2-pref" wire:click.prevent="goToThirdStep">Previous Step</button>
            </div>

        @endif
        @if($fifthStep)
            <div class="sixth-step  @if(!$fifthStep) d-none @endif">
                <div class="d-flex flex-row step-name">
                    <div class="d-flex flex-column content">
                        <div class="d-flex flex-row justify-content-start">
                            <h2>Fifth step</h2>
                        </div>
                        <div class="d-flex flex-row justify-content-start">
                            <p>Get Performance Plane </p>
                        </div>
                    </div>
                </div>
                <div class="plane">
                    <table class="table align-middle">
                        <tbody>
                        <tr class="">
                            <td>
                                Document
                            </td>
                            <td>
                                Performance Testing Plane For {{$testName}}
                            </td>
                        </tr>
                        <tr class="">
                            <td>
                                Test ID
                            </td>
                            <td>
{{--                                16431506305079946651--}}
                                {{$testID}}
                            </td>
                        </tr>
                        <tr class="">
                            <td>
                                Website
                            </td>
                            <td>
                                {{$website}}
                            </td>
                        </tr>
                        <tr class="">
                            <td>Objective</td>
                            <td>
                                Develop Performance test Plan for the {{$website}} website
                                to ensure if the current system architecture is optimal for handling increased
                                traffic on the website and also, to figure out if there are any upgrades that need to be
                                done.

                            </td>
                        </tr>
                        <tr class="">
                            <td>
                                Scope
                            </td>
                            <td>
                                1-Performing the user experience performance simulator without a specific load. <br/>
                                2-Performing the user experience performance simulator with a Expected Normal load (Load
                                Testing). <br/>
                                3-Performing the user experience performance simulator with a Expected Peak load (Peak
                                Load Testing). <br/>
                                4-Performing the user experience performance simulator with a Expected Future load
                                (Capacity Testing).
                                <br/>
                                5-Performing the user experience performance simulator with a Un Expected load (Spike
                                Testing). <br/>
                                6-Performing the user experience performance simulator with a Expected load During a
                                Long Time (Endurance
                                Testing). <br/>
                                7-Propose suggestions
                            </td>

                        </tr>

                        <tr class="">
                            <td>
                                Out of Scope
                            </td>
                            <td>
                                1-Functional or accuracy testing of the {{$website}} website.<br/>
                                2-Any other testing types not included in the Scope section.
                            </td>

                        </tr>
                        <tr class="">
                            <td>
                                Hardware
                            </td>
                            <td>
                                Load Generator, Ex: (Dell Intel Core i7 -3.40 GHz., 16GB RAM).<br/>

                            </td>

                        </tr>

                        <tr class="">
                            <td>
                                Software
                            </td>
                            <td>
                                Operating System, EX: (Windows Or Linux Or Mac)
                            </td>

                        </tr>

                        <tr class="">
                            <td>
                                Performance Testing Tool
                            </td>
                            <td>
                                1-GTmetrix.<br/>
                                2-Apache-JMeter-5.4.1.
                            </td>

                        </tr>

                        <tr class="">
                            <td>
                                Environment
                            </td>
                            <td>
                                1-Java Runtime Environment (JRE) is installed to run JMeter.<br/>
                                2-A good internet connection is Confirmed.

                            </td>

                        </tr>

                        <tr class="">
                            <td>
                                Risks
                            </td>
                            <td>
                                The load JMeter applying is not the only load for the web site.
                                Also, at the same time, there will be a website getting requests from the outside world.
                                So, the report analysis might be not very correct.
                                The Best Time To Execute This Plane After 11 PM.
                            </td>

                        </tr>

                        <tr class="">
                            <td>
                                Performance Test Scenarios
                            </td>
                            <td>
                                Scenarios are explained in: <br/>
                                1-Scenario 1 For One User.<br/>
                                2-Scenario 2 For Load Testing.<br/>
                                3-Scenario 3 For Peak Load Testing.<br/>
                                4-Scenario 4 For CapacityTesting.<br/>
                                5-Scenario 5 For Spike Testing.<br/>
                                6-Scenario 6 For Endurance Testing.<br/>
                            </td>
                        </tr>

                        <tr class="">
                            <td>
                                Entry Criteria
                            </td>
                            <td>
                                Functionally stable application.
                            </td>
                        </tr>
                        <tr class="">
                            <td>
                                Exit Criteria:
                            </td>
                            <td>
                                All the Performance objectives are met.
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="table align-middle">
                        <tbody>
                        <tr class="">
                            <td>Scenario 1</td>
                            <td>Test user Experience with out load</td>

                        </tr>

                        <tr class="">
                            <td>Objective</td>
                            <td>
                                Validate Response Time For All Pages in {{$website}} By measuring:<br/>
                                1-First Contentful Paint.<br/>
                                2-Time to Interactive.<br/>
                                3-Speed Index. <br/>
                                4-Total Blocking Time. <br/>
                                5-Largest Contentful Paint. <br/>
                                6-Cumulative Layout Shift.
                            </td>

                        </tr>

                        <tr class="">
                            <td> Performance Testing Tool</td>
                            <td>GTmetrix</td>
                        </tr>

                        <tr class="">
                            <td>Approach and Execution Strategy</td>
                            <td>
                                1-Select Any Browser.<br>
                                2-Go To https://gtmetrix.com/ <br/>
                                3-Enter URL For Target Page.
                            </td>

                        </tr>
                        <tr class="">
                            <td> Results</td>
                            <td>
                                Report For Every Page Contain ResponseTime Details.
                            </td>

                        </tr>
                        </tbody>
                    </table>

                    <table class="table align-middle">
                        <tbody>
                        <tr class="">
                            <td> Scenario 2</td>
                            <td> Test user Experience with Expected Normal Load</td>

                        </tr>

                        <tr class="">
                            <td>Objective</td>
                            <td> Validate Response Time For Every Page in {{$website}} </td>

                        </tr>

                        <tr class="">
                            <td>Performance Testing Tool</td>
                            <td>Apache-JMeter-5.4.1</td>
                        </tr>

                        <tr class="">
                            <td>Duration</td>
                            <td>
                                Ramp-Up period: {{$loadInfo['normal']['duration']*60}} seconds
                            </td>
                        </tr>

                        <tr class="">
                            <td>Approach and Execution Strategy</td>
                            <td>
                                Apply Workload modelling For Load testing On JMeter
                            </td>

                        </tr>
                        <tr class="">
                            <td> Results</td>
                            <td>
                                Report Contain Throughput and Response Time For Every Page.
                            </td>

                        </tr>
                        </tbody>
                    </table>

                    <table class="table align-middle">
                        <tbody>
                        <tr class="">
                            <td> Scenario 3</td>
                            <td> Test user Experience with Expected Peak Load</td>

                        </tr>

                        <tr class="">
                            <td>Objective</td>
                            <td> Validate Response Time For Every Page in {{$website}} </td>

                        </tr>

                        <tr class="">
                            <td>Performance Testing Tool</td>
                            <td>Apache-JMeter-5.4.1</td>
                        </tr>

                        <tr class="">
                            <td>Duration</td>
                            <td>
                                Ramp-Up period: {{$loadInfo['peak']['duration']*60 }} seconds
                            </td>
                        </tr>

                        <tr class="">
                            <td>Approach and Execution Strategy</td>
                            <td>
                                Apply Workload modelling For Peak testing On JMeter
                            </td>

                        </tr>
                        <tr class="">
                            <td> Results</td>
                            <td>
                                Report Contain Throughput and Response Time For Every Page.
                            </td>

                        </tr>
                        </tbody>

                    </table>

                    <table class="table align-middle">
                        <tbody>
                        <tr class="">
                            <td> Scenario 4</td>
                            <td> Test user Experience with Expected Future Load</td>

                        </tr>

                        <tr class="">
                            <td>Objective</td>
                            <td> Validate Response Time For Every Page in {{$website}} </td>
                        </tr>

                        <tr class="">
                            <td>Performance Testing Tool</td>
                            <td>Apache-JMeter-5.4.1</td>
                        </tr>

                        <tr class="">
                            <td>Duration</td>
                            <td>
                                Ramp-Up period: {{$loadInfo['future']['duration']*60}} seconds
                            </td>
                        </tr>

                        <tr class="">
                            <td>Approach and Execution Strategy</td>
                            <td>
                                Apply Workload modelling For Capacity testing On JMeter
                            </td>

                        </tr>
                        <tr class="">
                            <td> Results</td>
                            <td>
                                Report Contain Throughput and Response Time For Every Page.
                            </td>

                        </tr>
                        </tbody>
                    </table>

                    <table class="table align-middle">
                        <tbody>
                        <tr class="">
                            <td> Scenario 5</td>
                            <td> Test user Experience in Spike Testing</td>

                        </tr>

                        <tr class="">
                            <td>Objective</td>
                            <td> Validate Response Time For Every Page in {{$website}} And Server Resource Monitoring
                            </td>
                        </tr>

                        <tr class="">
                            <td>Performance Testing Tool</td>
                            <td>Apache-JMeter-5.4.1</td>
                        </tr>

                        <tr class="">
                            <td>Duration</td>
                            <td>
                                Ramp-Up period: {{$loadInfo['normal']['user']}} seconds
                            </td>
                        </tr>

                        <tr class="">
                            <td>Approach and Execution Strategy</td>
                            <td>
                                Apply Workload modelling For Load testing On JMeter
                            </td>

                        </tr>
                        <tr class="">
                            <td> Results</td>
                            <td>
                                Report Contain Throughput and Response Time For Every Page.
                            </td>

                        </tr>
                        </tbody>

                    </table>

                    <table class="table align-middle">
                        <tbody>
                        <tr class="">
                            <td> Scenario 6</td>
                            <td> Test user Experience with Expected Normal Load With Long Duration</td>

                        </tr>

                        <tr class="">
                            <td>Objective</td>
                            <td> Validate Response Time For Every Page in {{$website}} And Server Resource Monitoring
                            </td>
                        </tr>

                        <tr class="">
                            <td>Performance Testing Tool</td>
                            <td>Apache-JMeter-5.4.1</td>
                        </tr>

                        <tr class="">
                            <td>Duration</td>
                            <td>
                                Ramp-Up period: {{$loadInfo['endurance']['duration']*60}} seconds
                            </td>
                        </tr>

                        <tr class="">
                            <td>Approach and Execution Strategy</td>
                            <td>
                                Apply Workload modelling For Endurance testing On JMeter
                            </td>

                        </tr>
                        <tr class="">
                            <td> Results</td>
                            <td>
                                Report Contain Throughput and Response Time For Every Page.
                            </td>

                        </tr>
                        </tbody>
                    </table>
                    <table class=" table align-middle Workload">
                        <tbody>
                        <tr class="">
                            <td colspan="9" class="align-middle">
                                Workload modelling<br/>
                                NL: Number of Users in Load testing<br/>
                                NP: Number of Users in Peak Load testing<br/>
                                NC: Number of Users in Capacity testing<br/>
                                ND: Number of Users in Endurance testing<br/>
                                Z: Page Think Time (seconds)<br/>
                                P: Pacing (seconds)<br/>
                                L: Loop Count<br/>
                                TT: Time To End Iteration By One User (seconds)

                            </td>
                        </tr>
                        <tr class="">
                            <td style="border-right: none!important">Step</td>
                            <td style="border: none!important;"></td>
                            <td>NL</td>
                            <td>NP</td>
                            <td>NC</td>
                            <td>ND</td>
                            <td>P</td>
                            <td>L</td>
                            <td>TT</td>

                        </tr>
                        <?php $i = 1; ?>
                        @foreach($iterationsInFifth as $iteration)
                            <?php $timeForIterations = 0; ?>
                            <tr class="">
                                <td colspan="2" style="font-weight: normal!important;">
                                    <?php $g = 1; ?>
                                    @foreach($iteration['pages'] as $page)
                                        {{$g}}- &nbsp;{{$pagesInFifth[$page]['name']}} &nbsp;&nbsp;&nbsp;

                                        Z= {{$pagesInFifth[$page]['thinkTime']}} <br/>


                                        <?php $g++;
                                        $pageTime = $pagesInFifth[$page]['thinkTime'] + $pagesInFifth[$page]['responseTime'];
                                        $timeForIterations += $pageTime;
                                        ?>
                                    @endforeach
                                </td>
                                <td>{{round($iteration['normalUser'])}}</td>
                                <td>{{round($iteration['peakUser'])}}</td>
                                <td>{{round($iteration['futureUser'])}}</td>
                                <td>{{round($iteration['enduranceUser'])}}</td>
                                <td>{{$iteration['pacing']}}</td>
                                <td>{{$iteration['loopCount']}}</td>
                                <td>{{$timeForIterations+$iteration['pacing']}}</td>

                            </tr>
                            <?php $i++; ?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table align-middle">
                        <tbody>
                        <tr>
                            <td class="align-middle">
                                <p class="mr-2">Get Plane For Performance Testing</p>
                            </td>
                            <td class="align-middle">
                                <a href="{{route('test-plane-print')}}" class="btn btn-save-pref">
                                    Download
                                </a>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>

                <div class="d-flex flex-row justify-content-center mt-3">
                    <button class="btn  btn-edit-pref" wire:click.prevent="goToSixStep"> Next Step</button>
                </div>
                <div class="d-flex flex-row  justify-content-center mt-3">
                    <button class="btn  btn-cancle2-pref" wire:click.prevent="goToFourthStep">Previous Step</button>
                </div>
            </div>
        @endif

        <div class="seventh-step @if(!$sixthStep) d-none @endif">
            <div class="d-flex flex-row step-name">
                <div class="d-flex flex-column content">
                    <div class="d-flex flex-row justify-content-start">
                        <h2>Sixth step </h2>
                    </div>
                    <div class="d-flex flex-row justify-content-start">
                        <p> Generate Final Report For Your Test</p>
                    </div>
                </div>
            </div>

            <div class="container">

                <div
                    class="d-flex flex-column justify-content-start      @if($resultsFromJMeter or $resultsFromJTMitrex) d-none   @endif">
                    <div class="d-flex flex-column justify-content-start width-40 input-container">
                        <label class="form-label" for="form12">Enter Your Test ID</label>
                        <input type="text" id="form12"
                               class="form-control"
                               wire:model.defer="testIDReporting"/>
                        <button
                            wire:click.prevent="checkFromTestID()"
                            type="button"
                            class="btn btn-save-pref width-30 mt-3">
                            Save
                        </button>
                    </div>
                </div>


                @if($storedPages)
                    <div class="d-flex flex-column justify-content-start   @if(!$resultsFromJMeter) d-none @endif">

                        <div class="d-flex flex-row w-100">
                            <div class="d-flex flex-row justify-content-start page-navigation w-50">
                                <h4> Enter Result You Have Get From JMeter</h4>
                            </div>
                            <div class="d-flex flex-row justify-content-end w-50">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-edit-pref"
                                        data-bs-toggle="modal"
                                        data-bs-target="#staticBackdrop158">
                                    Show Saved Results
                                </button>

                                <!-- Modal -->
                                <div wire:ignore.self class="modal fade" id="staticBackdrop158"
                                     data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                                     aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header main-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Saved Results</h5>
                                                <button type="button" class="btn-close"
                                                        data-bs-dismiss="modal" aria-label="Close">

                                                </button>
                                            </div>
                                            <div class="modal-body d-flex flex-column justify-content-start">
                                                @foreach($addedJmeters as $page)
                                                    <div
                                                        class="d-flex flex-row justify-content-between align-items-center">
                                                        <div
                                                            class="d-flex flex-row justify-content-start align-items-center">
                                                            <i class="fa fa-check color-green mx-2"
                                                               aria-hidden="true"></i>
                                                            <p>{{$page['web_page']['pageName']}}
                                                                with {{$page['scenario']}}</p>
                                                        </div>
                                                        <button type="button" class="btn btn-danger"
                                                                wire:click.prevent="deleteJMeterResult({{$page['id']}})">
                                                            <i class="fa fa-window-close" aria-hidden="true"></i>
                                                        </button>
                                                    </div>

                                                @endforeach
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                                                    Close
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>


                        <form wire:submit.prevent="saveJMeterResult">
                            <div class="page ">
                                <div
                                    class="d-flex flex-row justify-content-start align-items-center page-navigation w-50">
                                    <p> Result For Page </p>
                                    <div class="d-flex w-50 p-2">
                                        <select class="form-control" id="exampleFormControlSelect2"
                                                wire:model.defer="selectedPageResultJmeter">
                                            <option>---</option>
                                            @foreach($storedPages as $key=>$page)
                                                <option value="{{$page['id']}}">{{$page['pageName']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="d-flex flex-row w-50">
                                    @error('selectedPageResultJmeter')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;
                                        </button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="d-flex flex-row justify-content-start w-100">
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label">Enter Min Response Time(Sec)</label>
                                        <input type="text" id="form13" class="form-control"
                                               wire:model.defer="minResponseTime"/>
                                        @error('minResponseTime')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label">Enter Max Response
                                            Time(Sec)</label>
                                        <input type="text" id="form13" class="form-control"
                                               wire:model.defer="maxResponseTime"/>
                                        @error('maxResponseTime')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label">Enter Median Response
                                            Time(Sec)</label>
                                        <input type="text" id="form13" class="form-control"
                                               wire:model.defer="medianResponseTime"/>
                                        @error('medianResponseTime')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-start w-100">
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label">Enter 90th pct Response
                                            Time(Sec)</label>
                                        <input type="text" id="form13" class="form-control"
                                               wire:model.defer="responseTime90th"/>
                                        @error('responseTime90th')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label">Enter 95th pct Response
                                            Time(Sec)</label>
                                        <input type="text" id="form13" class="form-control"
                                               wire:model.defer="responseTime95th"/>
                                        @error('responseTime95th')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label">Enter 99th pct Response
                                            Time(Sec)</label>
                                        <input type="text" id="form13" class="form-control"
                                               wire:model.defer="responseTime99th"/>
                                        @error('responseTime99th')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-start w-100">
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label">Enter Error Ratio .</label>
                                        <input type="text" id="form13" class="form-control"
                                               wire:model.defer="errorRatio"/>
                                        @error('errorRatio')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label">Enter Throughput
                                            (transaction/sec) </label>
                                        <input type="text" id="form13" class="form-control"
                                               wire:model.defer="throughput"/>
                                        @error('throughput')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label">Selected Scenario </label>
                                        <select class="form-control" id="exampleFormControlSelect2"
                                                wire:model.defer="selectedScenario">
                                            <option>---</option>
                                            <option>Load Testing</option>
                                            <option>Peak Load Testing</option>
                                            <option>Capacity Testing</option>
                                            <option>Spike Testing</option>
                                            <option>Endurance Testing</option>
                                        </select>
                                        @error('selectedScenario')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>


                                </div>

                                <div class="d-flex flex-row justify-content-between">
                                    <button
                                        type="submit"
                                        class="btn btn-save-pref width-10 mt-3">
                                        Save
                                    </button>

                                    <button
                                        type="button"
                                        wire:click.prevent="goToGTMetrixResults"
                                        class="btn  btn-edit-pref width-10 mt-3">
                                        Add GTmetrix Results
                                    </button>

                                </div>

                            </div>

                        </form>
                        <div class="d-flex flex-row justify-content-center">
                            @if(count($addedGTMitrics)==count($storedPages) and count($addedJmeters)==count($storedPages)*5)
                                <button type="button"
                                        wire:click.prevent="goToSeventhStep"
                                        class="btn btn-edit-pref">
                                    Get Final Report
                                </button>
                            @endif

                        </div>

                    </div>


                    <div
                        class="d-flex flex-column justify-content-start  @if(!$resultsFromJTMitrex)  d-none  @endif">

                        <div class="d-flex flex-row w-100">

                            <div class="d-flex flex-row justify-content-start page-navigation w-50">
                                <h4> Enter Result You Have Get From GTmetrix</h4>
                            </div>

                            <div class="d-flex flex-row justify-content-end w-50">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-edit-pref"
                                        data-bs-toggle="modal"
                                        data-bs-target="#staticBackdrop">
                                    Show Saved Results
                                </button>

                                <!-- Modal -->
                                <div wire:ignore.self class="modal fade" id="staticBackdrop" data-bs-backdrop="static"
                                     data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header main-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Saved Results</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                @foreach($addedGTMitrics as $page)
                                                    <div
                                                        class="d-flex flex-row justify-content-between align-items-center">
                                                        <div
                                                            class="d-flex flex-row justify-content-start align-items-center">
                                                            <i class="fa fa-check color-green mx-2"
                                                               aria-hidden="true"></i>
                                                            <p>{{$page['web_page']['pageName']}}</p>
                                                        </div>
                                                        <button type="button" class="btn btn-danger"
                                                                wire:click.prevent="deleteGTMetricsResult({{$page['web_page']['id']}})">
                                                            <i class="fa fa-window-close" aria-hidden="true"></i>
                                                        </button>
                                                    </div>

                                                @endforeach
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                                                    Close
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>


                        <form wire:submit.prevent="savePageResult">
                            <div class="page ">
                                <div
                                    class="d-flex flex-row justify-content-start align-items-center page-navigation w-50">
                                    <p> Result For Page </p>
                                    <div class="d-flex w-50 p-2">
                                        <select class="form-control" id="exampleFormControlSelect2"
                                                wire:model.defer="selectedPageResultGTMitrics">
                                            <option>---</option>
                                            @foreach($storedPages as $key=>$page)
                                                <option value="{{$page['id']}}">{{$page['pageName']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="d-flex flex-row w-50">
                                    @error('selectedPageResultGTMitrics')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;
                                        </button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="d-flex flex-row justify-content-start w-100">
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label" for="form12">Enter First Contentful Paint</label>
                                        <input type="text" id="form12" class="form-control"
                                               wire:model.defer="firstContentfulPaint"/>
                                        <select class="form-control" id="exampleFormControlSelect2"
                                                wire:model.defer="firstContentfulPaintDes">
                                            <option>---</option>
                                            <option>Good - nothing to do here</option>
                                            <option>OK, but consider improvement</option>
                                            <option>Longer than recommended</option>
                                            <option>Much longer than recommended</option>
                                        </select>
                                        @error('firstContentfulPaintDes')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                        @error('firstContentfulPaint')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label" for="form12">Enter Time to Interactive</label>
                                        <input type="text" id="form12" class="form-control"
                                               wire:model.defer="timeInteractive"/>
                                        <select class="form-control" id="exampleFormControlSelect2"
                                                wire:model.defer="timeInteractiveDes">
                                            <option>---</option>
                                            <option>Good - nothing to do here</option>
                                            <option>OK, but consider improvement</option>
                                            <option>Longer than recommended</option>
                                            <option>Much longer than recommended</option>
                                        </select>
                                        @error('timeInteractiveDes')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                        @error('timeInteractive')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label" for="form12">Enter Speed Index</label>
                                        <input type="text" id="form12" class="form-control"
                                               wire:model.defer="speedIndex"/>
                                        <select class="form-control" id="exampleFormControlSelect2"
                                                wire:model.defer="speedIndexDes">
                                            <option>---</option>
                                            <option>Good - nothing to do here</option>
                                            <option>OK, but consider improvement</option>
                                            <option>Longer than recommended</option>
                                            <option>Much longer than recommended</option>
                                        </select>
                                        @error('speedIndexDes')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                        @error('speedIndex')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>


                                </div>
                                <div class="d-flex flex-row justify-content-start w-100">
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label" for="form12">Enter Total Blocking Time</label>
                                        <input type="text" id="form12" class="form-control"
                                               wire:model.defer="blockingTime"/>
                                        <select class="form-control" id="exampleFormControlSelect2"
                                                wire:model.defer="blockingTimeDes">
                                            <option>---</option>
                                            <option>Good - nothing to do here</option>
                                            <option>OK, but consider improvement</option>
                                            <option>Longer than recommended</option>
                                            <option>Much longer than recommended</option>
                                        </select>
                                        @error('blockingTime')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                        @error('blockingTimeDes')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label" for="form12">Enter Largest Contentful
                                            Paint</label>
                                        <input type="text" id="form12" class="form-control"
                                               wire:model.defer="largestContentfulPaint"/>
                                        <select class="form-control" id="exampleFormControlSelect2"
                                                wire:model.defer="largestContentfulPaintDes">
                                            <option>---</option>
                                            <option>Good - nothing to do here</option>
                                            <option>OK, but consider improvement</option>
                                            <option>Longer than recommended</option>
                                            <option>Much longer than recommended</option>
                                        </select>
                                        @error('largestContentfulPaintDes')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                        @error('largestContentfulPaint')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label" for="form12">Enter Cumulative Layout
                                            Shift</label>
                                        <input type="text" id="form12" class="form-control"
                                               wire:model.defer="cumulativeLayoutShift"/>
                                        <select class="form-control" id="exampleFormControlSelect2"
                                                wire:model.defer="cumulativeLayoutShiftDes">
                                            <option>---</option>
                                            <option>Good - nothing to do here</option>
                                            <option>OK, but consider improvement</option>
                                            <option>Longer than recommended</option>
                                            <option>Much longer than recommended</option>
                                        </select>
                                        @error('cumulativeLayoutShiftDes')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                        @error('cumulativeLayoutShift')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="d-flex flex-row justify-content-start w-100">
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label" for="form12">Enter Performance Results </label>
                                        <input type="text" id="form12" class="form-control"
                                               wire:model.defer="GTPerformance"/>
                                        @error('GTPerformance')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror

                                    </div>
                                    <div
                                        class="d-flex flex-column justify-content-start width-30 p-2 input-container">
                                        <label class="form-label" for="form12">Enter Structure Results</label>
                                        <input type="text" id="form12" class="form-control"
                                               wire:model.defer="GTStructure"/>
                                        @error('GTStructure')
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;
                                            </button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>

                                </div>

                                <div class="form-group w-75">
                                    <label for="exampleFormControlTextarea1">Improvement For This Page</label>
                                    @foreach($improvementCounts as $counter)
                                        <div class="d-flex flex-row w-100">
                                            <input type="text" id="form12" class="form-control"
                                                   wire:model.defer="improvement.{{$counter}}"/>
                                            <button
                                                wire:click.prevent="addNewImprovement({{$counter}})"
                                                type="button"
                                                class="btn btn-save-pref width-10 mt-1  mx-2">
                                                Add
                                            </button>
                                        </div>
                                    @endforeach

                                    @error('improvement')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;
                                        </button>
                                        {{ $message }}</div>
                                    @enderror
                                    {{--                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="2"></textarea>--}}
                                </div>

                                <div class="d-flex flex-row justify-content-between">
                                    <button
                                        type="submit"
                                        class="btn btn-save-pref width-10 mt-3">
                                        Save
                                    </button>

                                    <button
                                        type="button"
                                        wire:click.prevent="goToJMeterResults"
                                        class="btn  btn-edit-pref width-10 mt-3">
                                        Add JMeter Results
                                    </button>

                                </div>


                            </div>
                        </form>

                        <div class="d-flex flex-row justify-content-center">
                            @if(count($addedGTMitrics)==count($storedPages) and count($addedJmeters)==count($storedPages)*5)
                                <button type="button"
                                        wire:click.prevent="goToSeventhStep"
                                        class="btn btn-edit-pref">
                                    Get Final Report
                                </button>
                            @endif

                        </div>

                    </div>

                @endif
            </div>
        </div>


        <div class="seventh-step @if(!$seventhStep) d-none @endif">
            <div class="d-flex flex-row step-name">
                <div class="d-flex flex-column content">
                    <div class="d-flex flex-row justify-content-start">
                        <h2>{{$websiteNameReporting}} </h2>
                    </div>
                    <div class="d-flex flex-row justify-content-start">
                        <p> Performance Testing Report</p>
                    </div>
                </div>
            </div>

            @if(count($storedResults)!=0)
                @foreach($storedResults as $pageResult)
                    @foreach($pageResult['page_j_meter_results'] as $jmeter)

                        @if($jmeter['scenario']=="Load Testing")
                            <div class="state">
                                <div class="d-flex flex-row step-name">
                                    <div class="d-flex flex-column content">
                                        <div class="d-flex flex-row justify-content-start">
                                            <h2>{{$pageResult['pageName']}}</h2>
                                        </div>
                                        <div class="d-flex flex-row justify-content-start">
                                            <p> Load Testing</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-container">
                                    <table class="table align-middle table-responsive-xl">
                                        <tbody>
                                        <tr class="">
                                            <td colspan="6">Response Time (Sec)</td>

                                            <td> Average</td>
                                            <td> Standard</td>
                                            <td> Deviation</td>
                                            <td>Error Ratio</td>
                                            <td> Throughput</td>

                                        </tr>
                                        <tr>
                                            <td>Min</td>
                                            <td>Median</td>
                                            <td>90th pct</td>
                                            <td>95th pct</td>
                                            <td>99th pct</td>
                                            <td>Max </td>

                                            <td></td>
                                            <td> Deviation</td>
                                            <td>From Expected</td>
                                            <td></td>
                                            <td>(transaction/sec)</td>

                                        </tr>

                                        <tr class="values">
                                            <td>{{$jmeter['min']}}  </td>
                                            <td>{{$jmeter['median']}} </td>
                                            <td>{{$jmeter['90th']}} </td>
                                            <td>{{$jmeter['95th']}}</td>
                                            <td>{{$jmeter['99th']}} </td>
                                            <td>{{$jmeter['max']}} </td>

                                            <td> {{$jmeter['average']}}  </td>
                                            <td> {{$jmeter['deviation']}}   </td>
                                            <td> {{$jmeter['deviation2']}}  </td>
                                            <td>{{$jmeter['error']}} </td>
                                            <td>{{$jmeter['throughput']}} </td>
                                        </tr>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        @endif
                        @if($jmeter['scenario']=="Peak Load Testing")
                            <div class="state">
                                <div class="d-flex flex-row step-name">
                                    <div class="d-flex flex-column content">
                                        <div class="d-flex flex-row justify-content-start">
                                            <h2>{{$pageResult['pageName']}}</h2>
                                        </div>
                                        <div class="d-flex flex-row justify-content-start">
                                            <p> Peak Load Testing</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-container">
                                    <table class="table align-middle table-responsive-xl">
                                        <tbody>
                                        <tr class="">
                                            <td colspan="6">Response Time (Sec)</td>

                                            <td> Average</td>
                                            <td> Standard</td>
                                            <td> Deviation</td>
                                            <td>Error Ratio</td>
                                            <td> Throughput</td>

                                        </tr>
                                        <tr>
                                            <td>Min</td>
                                            <td>Median</td>
                                            <td>90th pct</td>
                                            <td>95th pct</td>
                                            <td>99th pct</td>
                                            <td>Max</td>

                                            <td></td>
                                            <td> Deviation</td>
                                            <td>From Expected</td>
                                            <td></td>
                                            <td>(transaction/sec)</td>

                                        </tr>

                                        <tr class="values">
                                            <td>{{$jmeter['min']}}  </td>
                                            <td>{{$jmeter['median']}} </td>
                                            <td>{{$jmeter['90th']}} </td>
                                            <td>{{$jmeter['95th']}}</td>
                                            <td>{{$jmeter['99th']}} </td>
                                            <td>{{$jmeter['max']}} </td>

                                            <td> {{$jmeter['average']}}  </td>
                                            <td> {{$jmeter['deviation']}}   </td>
                                            <td> {{$jmeter['deviation2']}}  </td>
                                            <td>{{$jmeter['error']}} </td>
                                            <td>{{$jmeter['throughput']}} </td>
                                        </tr>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        @endif
                        @if($jmeter['scenario']=="Capacity Testing")
                            <div class="state">
                                <div class="d-flex flex-row step-name">
                                    <div class="d-flex flex-column content">
                                        <div class="d-flex flex-row justify-content-start">
                                            <h2>{{$pageResult['pageName']}}</h2>
                                        </div>
                                        <div class="d-flex flex-row justify-content-start">
                                            <p> Capacity Testing</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-container">
                                    <table class="table align-middle table-responsive-xl">
                                        <tbody>
                                        <tr class="">
                                            <td colspan="6">Response Time (Sec)</td>

                                            <td> Average</td>
                                            <td> Standard</td>
                                            <td> Deviation</td>
                                            <td>Error Ratio</td>
                                            <td> Throughput</td>

                                        </tr>
                                        <tr>
                                            <td>Min</td>
                                            <td>Median</td>
                                            <td>90th pct</td>
                                            <td>95th pct </td>
                                            <td>99th pct</td>
                                            <td>Max</td>

                                            <td></td>
                                            <td> Deviation</td>
                                            <td>From Expected</td>
                                            <td></td>
                                            <td>(transaction/sec)</td>

                                        </tr>
                                        <tr class="values">
                                            <td>{{$jmeter['min']}}  </td>
                                            <td>{{$jmeter['median']}} </td>
                                            <td>{{$jmeter['90th']}} </td>
                                            <td>{{$jmeter['95th']}}</td>
                                            <td>{{$jmeter['99th']}} </td>
                                            <td>{{$jmeter['max']}} </td>

                                            <td> {{$jmeter['average']}}  </td>
                                            <td> {{$jmeter['deviation']}}   </td>
                                            <td> {{$jmeter['deviation2']}}  </td>
                                            <td>{{$jmeter['error']}} </td>
                                            <td>{{$jmeter['throughput']}} </td>
                                        </tr>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        @endif
                        @if($jmeter['scenario']=="Spike Testing")
                            <div class="state">
                                <div class="d-flex flex-row step-name">
                                    <div class="d-flex flex-column content">
                                        <div class="d-flex flex-row justify-content-start">
                                            <h2>{{$pageResult['pageName']}}</h2>
                                        </div>
                                        <div class="d-flex flex-row justify-content-start">
                                            <p> Spike Testing</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-container">
                                    <table class="table align-middle table-responsive-xl">
                                        <tbody>
                                        <tr class="">
                                            <td colspan="6">Response Time (Sec)</td>

                                            <td> Average</td>
                                            <td> Standard</td>
                                            <td> Deviation</td>
                                            <td>Error Ratio</td>
                                            <td> Throughput</td>

                                        </tr>
                                        <tr>
                                            <td>Min</td>
                                            <td>Median</td>
                                            <td>90th pct</td>
                                            <td>95th pct</td>
                                            <td>99th pct</td>
                                            <td>Max</td>

                                            <td></td>
                                            <td> Deviation</td>
                                            <td>From Expected</td>
                                            <td></td>
                                            <td>(transaction/sec)</td>

                                        </tr>

                                        <tr class="values">
                                            <td>{{$jmeter['min']}}  </td>
                                            <td>{{$jmeter['median']}} </td>
                                            <td>{{$jmeter['90th']}} </td>
                                            <td>{{$jmeter['95th']}}</td>
                                            <td>{{$jmeter['99th']}} </td>
                                            <td>{{$jmeter['max']}} </td>

                                            <td> {{$jmeter['average']}}  </td>
                                            <td> {{$jmeter['deviation']}}   </td>
                                            <td> {{$jmeter['deviation2']}}  </td>
                                            <td>{{$jmeter['error']}} </td>
                                            <td>{{$jmeter['throughput']}} </td>
                                        </tr>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        @endif
                        @if($jmeter['scenario']=="Endurance Testing")
                            <div class="state">
                                <div class="d-flex flex-row step-name">
                                    <div class="d-flex flex-column content">
                                        <div class="d-flex flex-row justify-content-start">
                                            <h2>{{$pageResult['pageName']}}</h2>
                                        </div>
                                        <div class="d-flex flex-row justify-content-start">
                                            <p> Endurance Testing</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-container">
                                    <table class="table align-middle table-responsive-xl">
                                        <tbody>
                                        <tr class="">
                                            <td colspan="6">Response Time (Sec)</td>

                                            <td> Average</td>
                                            <td> Standard</td>
                                            <td> Deviation</td>
                                            <td>Error Ratio</td>
                                            <td> Throughput</td>

                                        </tr>
                                        <tr>
                                            <td>Min</td>
                                            <td>Median</td>
                                            <td>90th pct</td>
                                            <td>95th pct</td>
                                            <td>99th pct</td>
                                            <td>Max</td>

                                            <td></td>
                                            <td> Deviation</td>
                                            <td>From Expected</td>
                                            <td></td>
                                            <td>(transaction/sec)</td>

                                        </tr>

                                        <tr class="values">
                                            <td>{{$jmeter['min']}}  </td>
                                            <td>{{$jmeter['median']}} </td>
                                            <td>{{$jmeter['90th']}} </td>
                                            <td>{{$jmeter['95th']}}</td>
                                            <td>{{$jmeter['99th']}} </td>
                                            <td>{{$jmeter['max']}} </td>

                                            <td> {{$jmeter['average']}}  </td>
                                            <td> {{$jmeter['deviation']}}   </td>
                                            <td> {{$jmeter['deviation2']}}  </td>
                                            <td>{{$jmeter['error']}} </td>
                                            <td>{{$jmeter['throughput']}} </td>
                                        </tr>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        @endif
                    @endforeach


                    @if(count($pageResult['page_results'])!=0)

                        <div class="state">
                            <div class="d-flex flex-row step-name">
                                <div class="d-flex flex-column content">
                                    <div class="d-flex flex-row justify-content-start">
                                        <h2>{{$pageResult['pageName']}} </h2>
                                    </div>
                                    <div class="d-flex flex-row justify-content-start">
                                        <p> Main Performance Metrics </p>
                                    </div>
                                </div>
                            </div>
                            <div class="table-container px-2">
                                <div class="d-flex flex-row justify-content-between mt-2 flex-wrap">

                                    @foreach($pageResult['page_results'] as $p)
                                        <div class="card1 width-30">
                                            <div class="d-flex flex-row justify-content-between">
                                                <p>
                                                    {{$p['standard']}}
                                                </p>
                                                <p class="value">{{$p['value']}}</p>
                                            </div>
                                            @if(isset($p['details']))
                                            <br/>
                                            <p class="value-description">  {{$p['details']}}</p>
                                            @endif
                                        </div>
                                    @endforeach

                                </div>

                                @if(count($pageResult['page_results'])!=0)

                                    <div class="improvment">
                                        <div class="d-flex flex-row step-name">
                                            <div class="d-flex flex-column content width-30">
                                                <div class="d-flex flex-row justify-content-start">
                                                    <h2>Page Improvements</h2>
                                                </div>
                                            </div>
                                        </div>

                                        @foreach($pageResult['page_improvement'] as $improve)
                                            <p><i class="fa fa-check mx-2"
                                                  aria-hidden="true"></i>{{$improve['improvements']}}</p>
                                        @endforeach
                                    </div>
                                @endif

                            </div>
                        </div>
                    @endif

                @endforeach

{{--            {{dd($testInfo)}}--}}
{{--                <div class="d-flex flex-row justify-content-start my-2">--}}
{{--                    <a href="{{route('test-report-print',['testID'=>$testInfo->id])}}" class="btn btn-save-pref">--}}
{{--                        Download--}}
{{--                    </a>--}}
{{--                </div>--}}
            @endif


            <div class="d-flex flex-row justify-content-center mt-3">
                <button class="btn  btn-cancle2-pref" wire:click.prevent="goToSixStep">Previous Step</button>
{{--                <button class="btn  btn-cancle2-pref jj" onclick="window.print()">ttttt Step</button>--}}

            </div>
        </div>

    </div>
</div>
