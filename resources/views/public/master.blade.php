<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        @yield('title')
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<!-- Font Awesome -->
    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        rel="stylesheet"
    />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Arvo:ital,wght@1,700&family=Comfortaa:wght@300&family=Oxygen:wght@700&family=Patrick+Hand&display=swap" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Arvo:ital,wght@1,700&family=Bree+Serif&family=Comfortaa:wght@300&family=Oxygen:wght@700&family=Patrick+Hand&family=Patua+One&display=swap" rel="stylesheet">

{{--    content fonts--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Arvo:ital,wght@1,700&family=Bree+Serif&family=Comfortaa:wght@300&family=Encode+Sans&family=Oxygen:wght@700&family=Patrick+Hand&family=Patua+One&display=swap" rel="stylesheet">
    <!-- Google Fonts -->
{{--    <link--}}
{{--        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"--}}
{{--        rel="stylesheet"--}}
{{--    />--}}

    <!-- MDB -->
{{--    <link--}}
{{--        href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css"--}}
{{--        rel="stylesheet"--}}
{{--    />--}}
    @livewireStyles
{{--    <link href="{{asset('css/md.css')}}" rel="stylesheet">--}}

    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontAwsume.css')}}" rel="stylesheet">

</head>
{{--<livewire:template.nav-bar :country="$country" :city="$city"/>--}}
@yield('content')
{{--<livewire:template.footer :country="$country" :city="$city"/>--}}

<body>

<script>

</script>


<!-- MDB -->
{{--<script--}}
{{--    type="text/javascript"--}}
{{--    src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"--}}
{{--></script>--}}
<script src="{{asset('sweetAlert/sweetalert2.js')}}" defer></script>
<script src="{{asset('js/app.js')}}" defer></script>

@livewireScripts
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
{{--<script src="{{ asset('js/alpine.js') }}"></script>--}}



</body>

</html>
