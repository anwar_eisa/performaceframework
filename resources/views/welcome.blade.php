@extends('public.master')

@section('title')

   Index
@endsection

@section('content')

    <livewire:template.header-component />
    <livewire:template.home-component />

@endsection
