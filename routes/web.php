<?php

use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/About-Us', 'App\Http\Controllers\PublicController@showAboutPage')->name("About-Us");
Route::get('/', [PublicController::class ,'ShowHome'])->name("home");
Route::get('/test-plane-print/', [App\Http\Controllers\PrintPDFController::class, 'downLoadUserExperiencePlane'])->name('test-plane-print');
Route::get('/test-report-print/{testID}', [App\Http\Controllers\PrintPDFController::class, 'downLoadTestReport'])->name('test-report-print');
